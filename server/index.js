const { http } = require('./config');
const express = require('express')
const path = require('path')
const app = express()
const fileUpload = require('express-fileupload');

const ServerWebSocket = require('./ws');
const logger = require('./../modules/log.js')({ lim: 1, showTrace: false })


// Запускаем прослушивание
let server = app.listen(http.port)
console.log(`Сервер запущен, порт ${http.port}`);

ServerWebSocket(server)

// Включаем парсер параметров запроса
app.use(express.json({ limit: '50mb' }))
app.use(express.urlencoded({ limit: '50mb', extended: false }))

// Загрузчик файлов
app.use(fileUpload({
  limits: { fileSize: 50 * 1024 * 1024 },
}));

// Подключаем статические файлы
// app.use(express.static(`${__dirname}/static`));
app.use('/static', express.static(path.resolve(__dirname, `./static`)))
app.use(express.static(path.resolve(__dirname, `./../client/dist`)))
// app.use('/game', express.static(path.resolve(__dirname, `./../client/dist`)))

app.use('/docs', express.static(`${__dirname}/docs`));

app.use('/api', require(`./api`))

app.use((req, res) => {
  res.sendFile(path.resolve(__dirname, `./../client/dist/index.html`))
})
