// import MovableBlock from '../movable-block.js'
import FractionBlock from './../fraction/fraction-block.js'
import Storage from '../storages/storage.js'
import style from './../draw/style.js'
import { spawn } from './../draw/spawner.js'


import HealthBar from '../ui/health-bar.js'

export default class Character {
  health
  constructor() {


    // Здоровье персонажа
    this.healthMax = 100
    this.isAlive = true
  }

  get isAlive() {
    return !!this._isAlive
  }
  set isAlive(val) {
    return this._isAlive = !!val
  }

  death() {
    // Помечаем персонажа мертвым
    this.isAlive = false

    // Порождаем лут

    // Убираем данного персонажа
    this.desctruct()

  }

  tic(nearObjects) {
    this.animating()
    return super.tic(nearObjects)
  }
}
