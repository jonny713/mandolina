/** @module characters/play-character.js */

// import MovableBlock from './movable-block.js'
import Character from './character.js'
import Keys from './../inputs/keys.js'
import style from './../draw/style.js'

// import { playerFraction } from './characters/fraction.js'
// import Bullet from './bullets/bullet.js'

import Gun from './../guns/gun.js'
import Ammo from './../bullets/ammo.js'

/**
 * Персонаж с отслеживанием управления
 * @class
 * @extends Character
 */
export default class PlayCharacter extends Character {
  /**
   * Создание нового игрового персонажа
   * @param {object} start - объект с параметрами инициализации рисуемого объекта
   * @param {string} inputType - тип отслеживания направления - enum:direct,mouse
   * @param {Mouse} mouseObject - объект, отслеживающий состояние мыши
   */
  constructor(start) {
    super(start)
    console.log(`Во фракцию ${this.fraction.name} добавлен PlayCharacter с id ${this.id}`);


    this.keys = {
      up: Keys.find('up'),
      down: Keys.find('down'),
      left: Keys.find('left'),
      right: Keys.find('right')
    }

    this.style = style('char_idle')

    this.healthMax = 500


    // Даем на тест базовое кол-во выстрелов
    this.inventory.saveItems(new Ammo(), 999)

    // Устанавливаем в качесвте активного предмета экземпляр пушки
    this.activeItem = new Gun(this)
    return this
  }

  assignMouse(mouseObject) {
    this.mouseObject = mouseObject
  }

  calculateForces() {
    return {
      x: +this.keys.right.pressed - +this.keys.left.pressed,
      y: +this.keys.down.pressed - +this.keys.up.pressed,
    }
  }

  calculateDirection() {
    if (!this.mouseObject.inWorkSpace)
      return this._direction

    this._direction = Math.atan2(this.mouseObject.relativePosition.x - this.position.x, this.mouseObject.relativePosition.y - this.position.y) - Math.PI/2
    return this._direction
  }


  tic(nearObjects) {
    this.keys.up.value = parseInt(this.speed.y < 0 ? -this.speed.y : 0)
    this.keys.down.value = parseInt(this.speed.y > 0 ? this.speed.y : 0)

    this.keys.left.value = parseInt(this.speed.x < 0 ? -this.speed.x : 0)
    this.keys.right.value = parseInt(this.speed.x > 0 ? this.speed.x : 0)

    if (this.mouseObject.mouseDown) {
      this.useActiveItem()
    }

    return super.tic(nearObjects)
  }

	crossTrigger(/*crossObject*/) {
		// this.health = this.health || 100500
		// this.health -= crossObject.attack
		// console.log(this.health)
	}
}
