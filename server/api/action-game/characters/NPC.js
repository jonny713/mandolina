import Character from './character.js'
import FractionShield from './../ui/fraction-shield.js'
// import Ray from './../ray.js'
// import { monstersFraction } from './fraction.js'
import style from './../draw/style.js'
import { spawn } from './../draw/spawner.js'

import BadBigGun from './../guns/bad-big-gun.js'
import BadBigAmmo from './../bullets/bad-big-ammo.js'


export default class NPC extends Character {
  calculateDistanceToTarget(target) {
    return Math.sqrt((this.position.x - target.position.x) ** 2 + (this.position.y - target.position.y) ** 2)
  }

  constructor(initParam, /*relatedElement*/) {
    super(initParam)

    // monstersFraction.add(this.id)
    // this.relatedElement = relatedElement

    // this.isShooted = false
    this.attackDistance = 350
    // this.isEnemyArround = false
    this.lookDistance = 700
    // this.isPatrol = false

    this._mode = 'idle'
    // this.idleFrames = 20

    this._speed.max = 10

    // this.focus

    this.style = style('char_idle')
    // this.style = style(this.fraction.color)

    this.fractionShield = spawn(FractionShield, { x: -7, y: 0 }, this, this.fraction.color)



    // Даем на тест базовое кол-во выстрелов
    this.inventory.saveItems(new BadBigAmmo(), 999)

    // Устанавливаем в качесвте активного предмета экземпляр пушки
    this.activeItem = new BadBigGun(this)
  }

  get lookAroundFrames() {
    if (!this.frames)
      this.frames = {}

    if (!this.frames.lookAround)
      this.frames.lookAround = 20

    return --this.frames.lookAround
  }
  get idleFrames() {
    if (!this.frames)
      this.frames = {}

    if (!this.frames.idle)
      this.frames.idle = 20

    return --this.frames.idle
  }
  get patrolFrames() {
    if (!this.frames)
      this.frames = {}

    if (!this.frames.patrol)
      this.frames.patrol = 400

    return --this.frames.patrol
  }

  get mode() {
    return this._mode
  }
  set mode(newMode) {
    this.frames = {}
    this._mode = newMode
  }

  initStop() {
    this.moveTo(this.position, 4)
    delete this.patrolPosition
  }
  /**
  *  NPC осматривается вокруг в поисках цели
  *
  *  @returns {Object} enemy враг
  */
  lookArround() {
    for (let enemy of this.fraction.enemies) {
      // Отметаем тех, кто слишком далеко
      if (Math.abs(this.position.x - enemy.position.x) > this.lookDistance || Math.abs(this.position.y - enemy.position.y) > this.lookDistance) {
        continue
      }

      // Проверяем, что противник виден
      if (this.cell.isOverview(this.position, enemy.position)) {
        return this.focused = enemy
      }
    }
  }


  idleMode() {
    // Выполняем ожидание в течение нужного кол-ва кадров
    if (this.idleFrames) {
      return
    }

    // Включаем переход в патруль по-умолчанию
    this.mode = 'patrol'

    // Но проверяем вокруг, на всякий случай
    this.lookMode()
  }

  /*
  *  Режим патрулирования
  */
  patrolMode() {
    // Если координат патрулирования нет, считаем их
    if (!this.patrolPosition) {
      this.patrolPosition = (() => {
        let { position } = this.cell.rayCast(this.position, {
          x: this.position.x + Math.random() * 8e2 - 4e2,
          y: this.position.y + Math.random() * 8e2 - 4e2,
        })
        return position
      })()
    }

    // console.log('Двигаюсь к ', this.patrolPosition, '; Дистанция - ', Math.sqrt((this.patrolPosition.x - this.position.x)**2 + (this.patrolPosition.y - this.position.y)**2));
    // Отправляем по координатам
    this.moveTo(this.patrolPosition, 4)

    // Если координаты патрулирования достигнуты, удаляем их и меняем режим на "idle"
    if (this.cell.comparePosition(this.patrolPosition)) {
      this.mode = 'idle'
      return this.initStop()

    }

    // Выход из патруля
    if (!this.patrolFrames) {
      this.mode = 'idle'
      return this.initStop()
    }

    // Осмотримся
    this.lookMode()
  }


  /*
  *  Режим преследования
  */
  followMode() {
    let enemy = this.focused

    // Следуем за преследуемым
    this.moveTo(enemy.position)

    //
    this.lookMode()
  }

  /*
  *  Режим атаки
  */
  attackMode() {
    let enemy = this.focused

    // Целимся
    this.direction = -Math.atan2( (-this.position.y + enemy.position.y), (-this.position.x + enemy.position.x))

    // Стреляем
    this.useActiveItem()

    //
    this.lookMode()
  }

  /**
   * Режим осмотра
   */
  lookMode() {
    // Делаем задержку в несколько фреймов
    if (this.lookAroundFrames) {
      return
    }

    // Ищем противника
    let enemy = this.lookArround()
    if (enemy && enemy.isAlive) {
      // считаем дистанцию до него
      let distance = this.calculateDistanceToTarget(enemy)

      // Проверяем дистанцию до цели и выбираем соответсвующий режим
      if (distance < this.attackDistance) {
        // Если цель близко, включаем атаку
        this.mode = 'attack'
      } else
      if (distance < this.lookDistance) {
        // Если цель в обасти видимости, включаем преследование
        this.mode = 'follow'
      }

      return this.initStop()
    }

    // Если противник не найден и мы уже в патруле, остаемся в нем
    if (this.mode === 'patrol') {
      return
    }

    // В ином случае останавливаемся и уходим в ожидание
    this.mode = 'idle'
    return this.initStop()

  }

  /*
  *  Выбор текущего режима поведения
  */
  modeSelection() {
    switch (this.mode) {
      case 'idle':
        // console.log('Бездействую');
        this.idleMode()
        break;
      case 'patrol':
        // console.log('Патрулирую');
        this.patrolMode()
        break;
      case 'follow':
        // console.log('Преследую');
        this.followMode()
        break;
      case 'attack':
        // console.log('Атакую');
        this.attackMode()
        break;
      default:

    }
  }


  tic(nearObjects = []) {
    this.modeSelection()

    return super.tic(nearObjects)
  }

  takeDamage(attack, owner) {
    super.takeDamage(attack, owner)
    if (this.mode == 'patrol' || this.mode == 'idle') {
      this.mode = 'patrol'
      this.patrolPosition = owner.position
    }

  }
}
