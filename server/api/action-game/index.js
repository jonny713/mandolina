const { init, Game } = require('./../../ws/gameConnector');
if (typeof performance === 'undefined') {
	// Older Node.js
	global.performance = require('perf_hooks').performance;
} else {
	// Browser.
	global.performance = performance;
}
let ts = performance.now()
const TTL = 60 * 1e3
const GAMER_TTL = 30 * 1e3

/**
 * Основной класс игры. Каждый экземпляр соответствует одной игровой сессии или комнате
 * Любой подключенный клиент может вызвать любой метод данного класса (кроме помеченных @private или @protected)
 * Основная логика игры обрабатывается на мастер-клиенте (методы помечены @master)
 * Ведомые клиенты в основном присылают информацию об инпутах игроков
 *
 * Инфа накапливается какое то время и потом рассылается участникам
 */
class ActionGame extends Game {
	wave = 0
	ttlMax = TTL
	isAutoReply = false
	enemies = {}

	/**
	 * @protected Дополняем метод, срабатывающий при отключении всех игроков и закрытии комнаты
	 */
	delete() {
		super.delete()
		clearTimeout(this.timeoutID)
		console.log(`${this.code}: комната закрыта`);
	}

	/**
	 * @private
	 * @summary Вспомогательный метод для обновления gameObject'а
	 */
	updateGameObject(gameObject, data) {
		if (!gameObject) {
			return
		}
		let {
			coords,
			direction,
			health,
			activeItemName,
			isAttacked,
			focusedPos,
		} = data

		gameObject.coords = coords
		gameObject.direction = direction
		gameObject.health = health
		gameObject.activeItemName = activeItemName
		gameObject.isAttacked = isAttacked
		gameObject.focusedPos = focusedPos
		gameObject.updatedAt = new Date()
	}
	/**
	 * @protected
	 */
	constructor(master, channelName, code) {
		super(master, channelName, code)

		// Подключаем самого мастера как игрока
		this.connectGamer(master)

		// После подключения любого клиента
		this.tic()
	}

	/**
	 * @summary Вызывается игроком при старте
	 * @todo добавить защиту от переподключений
	 */
	start({ gamer: gamerName, healthMax, fraction, ...data }) {
		console.log(`${this.code}: подключен игрок ${gamerName}`);

		this.gamers[gamerName].active = true
		this.gamers[gamerName].fraction = fraction
		this.gamers[gamerName].healthMax = healthMax

		this.updateGameObject(this.gamers[gamerName], data)
	}

	/**
	 * @master
	 * @summary Инфа о новом противнике
	 */
	newEnemy({ id, healthMax, fraction, ...data }) {
		// console.log(`${this.code}: подключен враг ${id}`);

		if (!this.enemies[id]) {
			this.enemies[id] = { id }
		}
		this.enemies[id].fraction = fraction
		this.enemies[id].healthMax = healthMax

		this.updateGameObject(this.enemies[id], data)
	}

	/**
	 * @summary Инфа о выстреле
	 */
	newBullet({ id, healthMax, fraction, ...data }) {
		console.log(`${this.code}: Выстрел ${id}`);

		if (!this.enemies[id]) {
			this.enemies[id] = { id }
		}
		this.enemies[id].fraction = fraction
		this.enemies[id].healthMax = healthMax

		this.updateGameObject(this.enemies[id], data)
	}

	/**
	 * @summary Инфа о смещении игрока
	 */
	move({ gamer: gamerName, ...data }) {
		this.updateGameObject(this.gamers[gamerName], data)
	}

	/**
	 * @master
	 * @summary Смещение противников
	 * @todo добавить передачу нескольких противников за раз
	 */
	moveEnemy({ id, ...data }) {
		this.updateGameObject(this.enemies[id], data)
	}

	/**
	 * @summary сообщение об атаке
	 * @todo добавить передачу инфу о нескольких атаках за раз
	 */
	attack({ ownerId, gamerName }) {
		console.log(`${this.code}: атака ${gamerName || ownerId}`);

		this.send({
			action: 'attack',
			attack: {
				ownerId,
				gamerName,
			}
		})
	}

	/**
	 * @summary Отключение игрока
	 */
	disconnectGamer({ gamer: gamerName, }) {
		console.log(`${this.code}: Отключен игрок ${gamerName}`);
		this.gamers[gamerName].isAttacked = false
		this.gamers[gamerName].dead = true
	}

	/**
	 * @master
	 * @summary Удаление противника при победе над ним
	 */
	deleteEnemy({ id }) {
		console.log(`${this.code}: удален враг ${id}`);
		delete this.enemies[id]
	}

	/**
	 * @private
	 * @summary Метод, вызывающий сам себя раз в N мс и выполняющий основную логику (рассылку актуальной информации)
	 */
	tic() {
		// Проверяем, свежесть игрока. Если он долго не шлет пакетов, отключаем
		for (let g in this.gamers) {
			let gamer = this.gamers[g]
			if (!gamer.dead && new Date() - gamer.updatedAt > GAMER_TTL) {
				this.disconnectGamer({ gamer: g })
			}
		}

		// Если игроков не осталось, отключаем комнату
		if (Object.values(this.gamers).every(g => g.dead)) {
			this.delete()
			return
		}

		// Отправляем всем игрокам актуальное состояние игры
		this.send()

		// Ставим перевызов через секунду
		this.timeoutID = setTimeout(() => this.tic(), 100)

		let dts = performance.now() - ts
		if (dts  > 150) {
			console.log('ts', dts)
		}

		ts = performance.now()
	}

	/**
	 * @protected Заменяем реализацию метода, рассылающего данные по клиентам
	 * @todo попробовать отправлять игрокам только ту инфу, которая им нужна
	 */
	send(data = {}) {
		this.connection.send({
			code: this.code,
			gamers: Object.values(this.gamers),
			enemies: this.enemies,
		})
	}
}

init('action-game', ActionGame)
