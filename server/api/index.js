const express = require('express')
const router = express.Router()

router.get('/', (req, res) => {
  res.send(`API`)
})

router.use('/sprite-maps', require(`./sprite-maps`))

const tester = require('./tester');
const actionGame = require('./action-game');

module.exports = router
