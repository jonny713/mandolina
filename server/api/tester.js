const { init, Game } = require('./../ws/gameConnector');

class OneButtonGame extends Game {
  next() {
    // Очищаем все нажатия
    for (let gamer of Object.values(this.gamers)) {
      gamer.pressed = false
      gamer.firstPresser = false
    }
  }

  press({ gamer: gamerName }) {
    this.gamers[gamerName].pressed = true

    // Проверяем, первый ли
    for (let gamer of Object.values(this.gamers)) {
      if (gamer.firstPresser) {
        return
      }
    }

    this.gamers[gamerName].firstPresser = true

  }
}

init('tester', OneButtonGame)
