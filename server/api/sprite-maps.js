const path = require('path');
const express = require('express')
const router = express.Router()

const spriteMapsModel = require('./../libs/mongo/db').collection('spriteMaps');
const SPRITE_PATH = 'static/textures/sprites/'

/**
 * @api GET /api/sprite-maps/
 * @summary Получение списка
 */
router.get('/', async (req, res) => {
  try {
    const spriteMaps = await spriteMapsModel.find().toArray();
    console.log(spriteMaps);
    for (let spriteMap of spriteMaps) {
      spriteMap.src = `${SPRITE_PATH}${spriteMap.name}.png`
    }

    res.send({ success: true, spriteMaps })
  } catch (e) {
    // console.error(e);

    res.send({ success: false, message: e.message })
  }
})


/**
 * @api GET /api/sprite-maps/:spriteMapId
 * @summary Получение конкретной карты
 */
router.get('/:spriteMapId', async (req, res) => {
  try {
    let { spriteMapId } = req.params
    const spriteMap = await spriteMapsModel.findOne({ id: spriteMapId });

    res.send({ success: true, spriteMap })
  } catch (e) {
    console.error(e);

    res.send({ success: false, message: e.message })
  }
})


/**
 * @api POST /api/sprite-maps/
 * @summary Создание карты
 */
router.post('/', async (req, res) => {
  try {
    let { image } = req.files
    let spriteMap = JSON.parse(req.body.data)

    // TODO: Если запись в БД создастся, а файл не сохранится, то они разойдутся

    // Вычисляем новый id
    spriteMap.id = await spriteMapsModel.countDocuments()

    // Сохраняем в БД
    await spriteMapsModel.insertOne(spriteMap);

    // Формируем путь
    let uploadPath = path.join(__dirname, '../', SPRITE_PATH, image.name);
    // console.log(uploadPath);

    // Сохраняем файл
    image.mv(uploadPath, function(err) {
      if (err) {
        return res.status(500).send(err);
      }

      res.send({ success: true });
    });
  } catch (e) {
    console.error(e);

    res.send({ success: false, message: e.message })
  }
})

/**
 * @api POST /api/sprite-maps/:spriteMapId
 * @summary Обновление карты
 */
router.post('/:spriteMapId', async (req, res) => {
  try {
    let { spriteMapId } = req.params
    let { image } = req.files
    let spriteMap = JSON.parse(req.body.data)

    if (spriteMapId != spriteMap.id) {
      res.send({ success: false, message: 'Неверный id' })
    }

    // TODO: Если запись в БД обновится, а файл не сохранится, то они разойдутся

    // Обновляем запись
    await spriteMapsModel.updateOne(
      { id: spriteMap.id },
      { $set: { ...spriteMap }, },
      // { upsert: true },
    );

    // Формируем путь
    let uploadPath = path.join(__dirname, '../', SPRITE_PATH, image.name);
    console.log(uploadPath);

    // Сохраняем файл
    image.mv(uploadPath, function(err) {
      if (err) {
        return res.status(500).send(err);
      }

      res.send({ success: true });
    });

  } catch (e) {
    console.error(e);

    res.send({ success: false, message: e.message })
  }
})

/**
 * @api DELETE /api/sprite-maps/:spriteMapId
 * @summary Удаление карты
 * @todo Возможно не нужно
 */
router.delete('/:spriteMapId', async (req, res) => {
  try {
    // Решаем, нужно ли

    res.send({ success: true })
  } catch (e) {
    console.error(e);

    res.send({ success: false, message: e.message })
  }
})


module.exports = router
