/**
* Подключаем параметры для http сервера
*/
const {
  MONGO_NAME,
  MONGO_PASS,
  MONGO_IP,
} = process.env


/**
* Проверка наличия необходимых параметров
*/

module.exports = {
  name: MONGO_NAME,
  pass: MONGO_PASS,
  ip: MONGO_IP,
}
