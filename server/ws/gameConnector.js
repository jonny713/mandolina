const ServerWebSocket = require('./index');

let genRoom = (function*() {
	let c = 0
	while (true) {
		yield `${c}`.padStart(4, '0')
		c++
	}
})()
const games = {}

// Стандартное время жизни
const TTL = 20 * 60 * 1e3

class Game {
	static check() {
		return true
	}

	master = ''
	gamers = {}

	ttlMax = TTL
	ttl = this.ttlMax
	isAutoReply = true
	checkLive() {
		let ticTime = 10 * 1e3

		if (this.ttl >= 0) {
			this.ttl -= ticTime
			this.ttlTimeoutID = setTimeout(() => this.checkLive(), ticTime)
			return
		}

		games[this.code].delete()
	}

	delete() {
		clearTimeout(this.ttlTimeoutID)
		delete games[this.code]
	}

	constructor(master, channelName, code) {
		this.master = master
		this.code = code

		console.log(`${channelName}:${code}`);
		this.connection = ServerWebSocket().subscribe(`${channelName}:${code}`)
		this.connection.onMessage(({ code, action, ...args }) => {
			this.ttl = this.ttlMax
			let result
			if (this[action]) {
				result = this[action]({ ...args })
			}

			result = result || {};

			if (this.isAutoReply) {
				this.connection.send({
					master: this.master,
					gamers: Object.values(this.gamers),
					code,
					...result,
				})
			}
		})
	}

	connectGamer(gamerName) {
		this.gamers[gamerName] = {
			name: gamerName,
		}
	}

	send(data = {}) {
		this.connection.send({
			master: this.master,
			gamers: Object.values(this.gamers),
			code: this.code,
			...data,
		})
	}

}

module.exports.Game = Game
module.exports.init = (channelName, GameConstructor) => {
	if (!GameConstructor.check()) {
		throw new Error(`${GameConstructor.name} is not instance of Game`)
	}
	let subscriber = ServerWebSocket().subscribe(channelName)
	subscriber.onMessage(({ code, gamer, action, answer }) => {
		if (!gamer) {
			console.log('Подключение без имени пользователя');
			answer({ message: `Отсутствует обязательное поле gamer` })
			return
		}

		if (action !== 'connect') {
			console.log(`Неизвестный action:${action}`);
			answer({ message: `Неизвестный action:${action}` })
			return
		}

		if (code && !games[code]) {
			console.log(`Неизвестный code:${code}`);
			// answer({ message: `Неизвестный code:${code}` })
			code = genRoom.next().value
			games[code] = new GameConstructor(gamer, channelName, code)
			console.log(`Создана игра с code:${code}`);
			// Ставим автоудаление
			games[code].checkLive()
			answer({ code })
			return
		}

		if (!code) {
			code = genRoom.next().value
			games[code] = new GameConstructor(gamer, channelName, code)
			console.log(`Создана игра с code:${code}`);
			// Ставим автоудаление
			games[code].checkLive()
			answer({ code })
			return
		}

		games[code].connectGamer(gamer)
		answer({
			master: games[code].master,
			gamers: Object.values(games[code].gamers),
			code,
		})

	})
}
