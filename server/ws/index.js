const WebSocket = require('ws')

class ServerWebSocket {
  callbacks = {}
  connections = {}
  ready = false
  constructor(httpsServer) {
    this.wss = new WebSocket.Server({ server: httpsServer });
    this.ready = true

    this.wss.on('connection', (ws) => {

      ws.on('message', (event) => {

        const { type, ...data } = JSON.parse(event.toString())
        // console.log('received: ', type, data);

        if (data.action === 'subscribe') {
          if (!this.connections[type]) {
            this.connections[type] = []
          }

          this.connections[type].push(ws)
        }

        if (this.callbacks[type]) {
          for (let cb of this.callbacks[type]) {

            cb({
              ...data,
              answer: (data) => {
                this.wsSend({
                  type,
                  ...data,
                }, ws)
              }
            })
          }
        }
      });
    });
  }

  wsSend(data, connection) {
    // console.log('sended: ', data);

    if (connection) {
      return connection.send(JSON.stringify(data));
    }
    if (!data.type || !this.connections[data.type]) {
      // console.log('send error: ', data);
      return
    }

    for (let connection of this.connections[data.type]) {
      connection.send(JSON.stringify(data));
    }

  }

  subscribe(type, filter = []) {
    const filtered = {}

    return {
      send: (data) => {
        this.wsSend({
          type,
          ...data,
        })
      },
      onMessage: (cb) => {
        if (typeof cb !== 'function') {
          return
        }

        if (!this.callbacks[type]) {
          this.callbacks[type] = []
        }

        if (filter.length) {
          this.callbacks[type].push((data, ws) => {

            cb
          })

          return
        }
        this.callbacks[type].push(cb)
      },
    }
  }
}

let serverWebSocket
module.exports = (data) => {
  if (serverWebSocket) {
    return serverWebSocket
  }

  return serverWebSocket = new ServerWebSocket(data)
}
