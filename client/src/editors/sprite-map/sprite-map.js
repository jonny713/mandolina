/**
 * @summary
 */
export class SpriteMap {
  constructor({ id, name, src, sprites = [] }) {
    this.id = id
    this.name = name
    this.spriteList = sprites

    let filledCells = { row: 0, col: 0 }
    this.sprites = {}

    for (let sprite of sprites) {
      for (let cell of sprite.cells) {
        if (!this.sprites[cell[0]]) {
          this.sprites[cell[0]] = {}
        }
        this.sprites[cell[0]][cell[1]] = sprite
      }
      filledCells.row = Math.max(...sprite.cells.map(cell => cell[0]), filledCells.row)
      filledCells.col = Math.max(...sprite.cells.map(cell => cell[1]), filledCells.col)
    }

    this.rows = filledCells.row + 1
    this.cols = filledCells.col + 1


    let image = new Image();
    if (!src) {
      this.image = image
      return this
    }
    image.src = src;
    image.crossOrigin = 'Anonymous';
    image.onload = () => {
      this.image = image
      this.cellSize = image.width / this.cols
    }
  }

  async ready() {
    let c = 1000
    while (!this.image || c == 0) {
      c--
      await new Promise(resolve => setTimeout(resolve, 5))
    }
  }

  /**
   * @summary Дает информацию о спрайте по координатам
   */
  getInfo({ row, col }) {
    if (this.sprites[row]) {
      return this.sprites[row][col]
    }
  }

  /**
   * @summary Получает на вход новый спрайт, проверяет его ячейки
   */
  toSave(sprite) {
    if (!sprite) {
      return null
    }

    let { name, cells, blocked } = sprite

    // Проверяем уникальность имени
    for (let sprite of this.spriteList) {
      if (sprite.name === name) {
        console.log(`Не уникальное имя ${name}, сохранение невозможно`);
        return null
      }
    }

    // Провеям пересечения
    for (let [row, col] of cells) {
      if (this.getInfo({ row, col })) {
        console.log(`Найдено пересечение в координатах ${[row, col]}, сохранение невозможно`);
        return null
      }
    }

    let sprites = [...this.spriteList]
    sprites.push({ name, cells, blocked })

    return {
      id: this.id,
      name: this.name,
      src: this.src,
      sprites
    }
  }
}
