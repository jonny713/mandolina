import { SpriteMapDrawer } from './drawer'

async function initEditor({ canvas, saveCanvas }) {
  const drawer = new SpriteMapDrawer(canvas, saveCanvas)

  return drawer
}

export default initEditor
