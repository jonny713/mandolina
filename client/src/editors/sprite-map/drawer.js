export class SpriteMapDrawer {
  width = 1000
  height = 600

  spriteMap = {
    x: 0,
    y: 0,
    width: 0,
    height: 0,
    image: null
  }
  sprite = {
    x: 0,
    y: 0,
    width: 0,
    height: 0,
    image: null
  }
  selectedCells = {}

  lightCell = true
  enabledCell = true
  // switchCell = () => this.lightCell = !this.lightCell
  switchCellLighted() { return this.lightCell = !this.lightCell }
  switchCellEnabled() { return this.enabledCell = !this.enabledCell }

  constructor(canvas, saveCanvas) {
    this.canvas = canvas
    this.saveCanvas = saveCanvas
    this.canvas.width  = this.width
    this.canvas.height = this.height

    let ctx = canvas.getContext('2d')
    let saveCtx = saveCanvas.getContext('2d')
    this.ctx = new Proxy({}, {
      get(target, prop, receiver) {
        if (prop in ctx && typeof ctx[prop] == 'function') {
          return (...args) => {
            if (['drawImage'].includes(prop)) {
              saveCtx[prop](...args)
            }
            let result = ctx[prop].apply(ctx, [...args])
            return result
          }
        }
        if (prop in ctx) {
          return ctx[prop]
        }
        return Reflect.get(...arguments);
      },
      set(target, prop, value, receiver) {
        if (prop in ctx) {
          saveCtx[prop] = value
          ctx[prop] = value
        }
        return Reflect.set(...arguments);
      }
    })

    const proxy = new Proxy(this, {
      get(target, prop) {
        if (prop in target && typeof target[prop] == 'function') {
          return (...args) => {
           target.preDraw()
           let result = target[prop](...args)
           target.postDraw()
           return result
         }
        }
        return target[prop]
      }
    })

    this.canvas.addEventListener('mousemove', e => proxy.onHover(e))
    this.canvas.addEventListener('click', e => proxy.onClick(e))
    return proxy
  }
  drawImage({ image, sx, sy, sWidth, sHeight, dx, dy, dWidth, dHeight, status }) {
    if (image) {
      this.ctx.drawImage(image, sx, sy, sWidth, sHeight, dx, dy, dWidth, dHeight)
    }

    if (!this.lightCell) {
      return
    }

    this.ctx.lineWidth = 1
    this.ctx.strokeRect(dx, dy, dWidth, dHeight);
    this.ctx.fillStyle =  status === 'none' ? 'rgba(20, 20, 20, .1)' :
                          status === 'new' ? 'rgba(20, 240, 20, .1)' :
                          status === 'blocked' ? 'rgba(240, 20, 20, .1)' :
                          'rgba(135, 206, 250, .1)'
    this.ctx.fillRect(dx, dy, dWidth, dHeight);
  }

  getCellByCoords(e) {
    if (!this.cells) {
      return null
    }

    let canvasX = (e.x - e.target.offsetLeft) / (e.target.clientWidth / this.width)
    let canvasY = (e.y - e.target.offsetTop) / (e.target.clientHeight / this.height)

    return this.cells.find(({ dx, dy, dWidth, dHeight }) => {
      return dx < canvasX && dx + dWidth >= canvasX &&
      dy < canvasY && dy + dHeight >= canvasY
    })
  }

  onHover(e) {
    let cell = this.getCellByCoords(e)
    if (!cell) {
      return null
    }
    let sprite = this.spriteMap.original.getInfo(cell)
    // TODO: стоит сделать pop-up, который будет отображать мета-инфу о спрайте
    // console.log(sprite);
  }
  onClick(e) {
    let cell = this.getCellByCoords(e)

    if (!cell) {
      return null
    }

    if (!['new', 'blocked', 'none'].includes(cell.status)) {
      return null
    }

    if (cell.status === 'none') {
      this.selectedCells[cell.row] = this.selectedCells[cell.row] || {}
      this.selectedCells[cell.row][cell.col] = { status: this.enabledCell ? 'new': 'blocked' }

      cell.status = this.enabledCell ? 'new': 'blocked'
      this.updateCellsCount({ row: cell.row + 1, col: cell.col + 1 })
    } else {
      delete this.selectedCells[cell.row][cell.col]
      if (!Object.keys(this.selectedCells[cell.row]).length) {
        delete this.selectedCells[cell.row]
      }

      cell.status = 'none'
      this.updateCellsCount({ row: cell.row, col: cell.col })
    }
  }

  assignMap(spriteMap) {
    this.spriteMap = {
      rows: spriteMap.rows + 2,
      cols: spriteMap.cols,
      original: spriteMap
    }

    this.recalculateCells()
    this.updateCellsCount()
  }

  recalculateCells() {
    // Формируем массив ячеек, которые будем рисовать
    let { rows, cols, original } = this.spriteMap

    // Пересчитываем размер ячейки
    let cellSize = this.spriteMap.cellSize = this.height / rows
    let originalCellSize = this.height / original.rows

    // Добавляем ячейки существующей карты
    let cells = []
    for (let row = 0; row < rows; row++) {
      for (let col = 0; col < cols; col++) {
        let sprite = original.getInfo({ row, col })
        let selected = this.selectedCells[row] ? this.selectedCells[row][col] : null

        if (selected) {
          let left = Math.min(...Object.values(this.selectedCells).map(row => Object.keys(row)).flat())
          let right = Math.max(...Object.values(this.selectedCells).map(row => Object.keys(row)).flat())

          let selectedWidth = right - left + 1

          let top = Math.min(...Object.keys(this.selectedCells))
          let bottom = Math.max(...Object.keys(this.selectedCells))
          let selectedHeight = bottom - top + 1

          let sx = col - left
          let sy = row - top

          cells.push({
            col,
            row,
            image: this.sprite.image,
            sx: (col - left) * this.sprite.image?.width / selectedWidth,
            sy: (row - top) * this.sprite.image?.height / selectedHeight,
            sWidth: this.sprite.image?.width / selectedWidth,
            sHeight: this.sprite.image?.height / selectedHeight,
            dx: col * cellSize,
            dy: row * cellSize,
            dWidth: cellSize,
            dHeight: cellSize,
            status: selected.status
          })
        } else {
          cells.push({
            col,
            row,
            image: sprite ? original.image : null,
            sx: col * original.image.width / original.cols,
            sy: row * original.image.height / original.rows,
            sWidth: original.image.width / original.cols,
            sHeight: original.image.height / original.rows,
            dx: col * cellSize,
            dy: row * cellSize,
            dWidth: cellSize,
            dHeight: cellSize,
            status: sprite ? sprite.status : 'none'
          })
        }

      }
    }

    this.cells = cells
  }

  updateCellsCount() {
    // Перебираем все ячейки и отрезаем лишние строки и столбцы
    this.spriteMap.rows = 2 + Math.max(0, ...this.cells
      .filter(({ status }) => status !== 'none')
      .map(({ row }) => row)
    )

    this.spriteMap.cols = 2 + Math.max(0, ...this.cells
      .filter(({ status }) => status !== 'none')
      .map(({ col }) => col)
    )

    this.recalculateCells()
  }

  assignSprite(img) {
    this.sprite.image = img

    this.recalculateCells()
    // this.sprite.image = img
    // this.sprite.cells =
    // this.sprite.x = this.spriteMap.width - this.spriteMap.cell.width
    // this.sprite.y = this.spriteMap.y
  }

  preDraw(){
    // Очищаем
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
  }
  postDraw(){
    if (!this.cells) {
      return null
    }

    // Меняем размеры холста для сохранения
    this.saveCanvas.width = (this.spriteMap.cols - 1) * this.spriteMap.cellSize
    this.saveCanvas.height = (this.spriteMap.rows - 1) * this.spriteMap.cellSize

    // Рисуем тайлы
    for (let cell of this.cells) {
      this.drawImage(cell)

    }

  }

  toSave(name) {
    let cells = []
    let blocked = []

    for (let cell of this.cells) {
      if (cell.status === 'blocked') {
        cells.push([ cell.row, cell.col ])
        blocked.push([ cell.row, cell.col ])
      }

      if (cell.status === 'new') {
        cells.push([ cell.row, cell.col ])
      }
    }

    return this.spriteMap.original.toSave({ name, cells, blocked })
  }
}
