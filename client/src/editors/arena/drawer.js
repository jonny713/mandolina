import { generate } from './generator'

export class ArenaDrawer {
  width = 1000
  height = 600
  generated = null

  constructor(canvas, saveCanvas) {
    this.canvas = canvas
    this.saveCanvas = saveCanvas
    this.canvas.width  = this.width
    this.canvas.height = this.height

    this.ctx = canvas.getContext('2d')

    const proxy = new Proxy(this, {
      get(target, prop) {
        if (prop in target && typeof target[prop] == 'function') {
          return (...args) => {
           target.preDraw()
           let result = target[prop](...args)
           target.postDraw()
           return result
         }
        }
        return target[prop]
      }
    })

    // this.canvas.addEventListener('mousemove', e => proxy.onHover(e))
    // this.canvas.addEventListener('click', e => proxy.onClick(e))
    return proxy
  }

  drawImage({ image, sx, sy, sWidth, sHeight, dx, dy, dWidth, dHeight, status }) {
    // console.log({  sx, sy, sWidth, sHeight, dx, dy, dWidth, dHeight, status });
    if (image) {
      this.ctx.drawImage(image, sx, sy, sWidth, sHeight, dx, dy, dWidth, dHeight)
    }

    if (!this.lightCell) {
      return
    }

    this.ctx.lineWidth = 1
    this.ctx.strokeRect(dx, dy, dWidth, dHeight);
    this.ctx.fillStyle =  status === 'none' ? 'rgba(20, 20, 20, .1)' :
                          status === 'new' ? 'rgba(20, 240, 20, .1)' :
                          status === 'blocked' ? 'rgba(240, 20, 20, .1)' :
                          'rgba(135, 206, 250, .1)'
    this.ctx.fillRect(dx, dy, dWidth, dHeight);
  }

  getCellByCoords(e) {
    if (!this.cells) {
      return null
    }

    let canvasX = (e.x - e.target.offsetLeft) / (e.target.clientWidth / this.width)
    let canvasY = (e.y - e.target.offsetTop) / (e.target.clientHeight / this.height)

    return this.cells.find(({ dx, dy, dWidth, dHeight }) => {
      return dx < canvasX && dx + dWidth >= canvasX &&
      dy < canvasY && dy + dHeight >= canvasY
    })
  }

  onHover(e) {
    let cell = this.getCellByCoords(e)
    if (!cell) {
      return null
    }
    let sprite = this.spriteMap.original.getInfo(cell)
    // TODO: стоит сделать pop-up, который будет отображать мета-инфу о спрайте
    // console.log(sprite);
  }
  onClick(e) {
    let cell = this.getCellByCoords(e)

    if (!cell) {
      return null
    }

    if (!['new', 'blocked', 'none'].includes(cell.status)) {
      return null
    }

    if (cell.status === 'none') {
      this.selectedCells[cell.row] = this.selectedCells[cell.row] || {}
      this.selectedCells[cell.row][cell.col] = { status: this.enabledCell ? 'new': 'blocked' }

      cell.status = this.enabledCell ? 'new': 'blocked'
      this.updateCellsCount({ row: cell.row + 1, col: cell.col + 1 })
    } else {
      delete this.selectedCells[cell.row][cell.col]
      if (!Object.keys(this.selectedCells[cell.row]).length) {
        delete this.selectedCells[cell.row]
      }

      cell.status = 'none'
      this.updateCellsCount({ row: cell.row, col: cell.col })
    }
  }

  generate(size, spriteMap) {
    let { background, width, height, border, objects } = generate(size, spriteMap)

    let koef = Math.min(...[
      this.width / width,
      this.height / height
    ])

    // console.log(`koef:${koef}`);

    this.generated = {
      background, width, height, border,
      objects: objects.map(({ sprite, dx, dy, dWidth, dHeight }) => {
        return {
          sprite,
          dx: dx * koef,
          dy: dy * koef,
          dWidth: dWidth * koef,
          dHeight: dHeight * koef,
        }
      })
    }
  }

  preDraw(){
    // Очищаем
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
  }
  postDraw(){
    if (!this.generated) {
      return null
    }

    let koef = Math.min(...[
      this.width / this.generated.width,
      this.height / this.generated.height
    ])

    // Рисуем тайлы
    for (let object of this.generated.objects) {
      this.drawImage({
        ...object.sprite,
        ...object,
      })

    }

  }

  toSave(name) {
    let cells = []
    let blocked = []

    for (let cell of this.cells) {
      if (cell.status === 'blocked') {
        cells.push([ cell.row, cell.col ])
        blocked.push([ cell.row, cell.col ])
      }

      if (cell.status === 'new') {
        cells.push([ cell.row, cell.col ])
      }
    }

    return this.spriteMap.original.toSave({ name, cells, blocked })
  }
}
