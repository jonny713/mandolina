let _sprites = []

/**
 * @summary
 * @param {number[]} chances шансы появления
 */
function getRandIndex(chances = [4, 2, 10]) {
  let rand = Math.random() * 100
  for (let c = 0; c < chances.length; c++) {
    if (rand < chances[c]) {
      return c
    }
    rand -= chances[c]
  }

  return null
}

export const generate = (size, sprites) => {
  let width = size.width || 4000
  let height = size.height || 2000
  _sprites = []

  for (let { name, cells, image, cellSize } of sprites) {
    let sx = cellSize * cells.reduce((a,cell) => a = Math.min(cell[1], a), Infinity)
    let sy = cellSize * cells.reduce((a,cell) => a = Math.min(cell[0], a), Infinity)
    let sWidth = cellSize * (1 + cells.reduce((a,cell) => a = Math.max(cell[1], a), 0)) - sx
    let sHeight = cellSize * (1 + cells.reduce((a,cell) => a = Math.max(cell[0], a), 0)) - sy

    _sprites.push({
      name,
      image,
      sx,
      sy,
      sWidth,
      sHeight,
      cellSize,
    })
  }

  let minCellSize = Math.min(...sprites.map(({ cellSize }) => cellSize))

  let rows = Math.round(height / (minCellSize * 1.2))
  let cols = Math.round(width / (minCellSize * 1.2))
  let objects = []

  let chances = sprites.map(sprite => sprite.chance)

  for (let row = 0; row <= rows; row++) {
    for (let col = 0; col <= cols; col++) {
      let index = getRandIndex(chances)

      if (index === null) {
        continue
      }

      objects.push({
        sprite: _sprites[index],
        dx: (col + Math.random() - .3) * minCellSize,
        dy: (row + Math.random() - .3) * minCellSize,
        dWidth: _sprites[index].sWidth * minCellSize / _sprites[index].cellSize,
        dHeight: _sprites[index].sHeight * minCellSize / _sprites[index].cellSize,
      })
    }
  }

  console.log(`objects:`, objects);

  return {
    background: 'grey',
    width,
    height,
    border: 'd="M 123.679 76.756 C 124.051 69.308 151.661 63.798 157.24 115.703 C 161.853 158.617 150.622 178.904 186.658 170.811 C 222.694 162.718 218.977 187.798 218.977 187.798 C 218.977 187.798 247.98 317.484 278.227 245.39 C 308.474 173.296 237.207 244.562 237.207 244.562 C 237.207 244.562 229.546 274.05 235.964 276.88 C 242.382 279.71 240.091 296.259 231.407 305.47 C 222.723 314.681 184.395 338.525 185.83 329.915"',
    objects,
    // : [{
    //   sprite: sprites[2],
    //   dx: 1032,
    //   dy: 57,
    // },{
    //   sprite: sprites[2],
    //   dx: 262,
    //   dy: 270,
    // },{
    //   sprite: sprites[2],
    //   dx: 690,
    //   dy: 770,
    // },{
    //   sprite: sprites[1],
    //   dx: 1190,
    //   dy: 670,
    // },{
    //   sprite: sprites[0],
    //   dx: 50,
    //   dy: 111,
    // }]
  }
}
