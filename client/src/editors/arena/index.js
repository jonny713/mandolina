import { ArenaDrawer } from './drawer'
import { wait } from './generator'

async function initEditor({ canvas }) {
  const drawer = new ArenaDrawer(canvas)

  return drawer
}

export default initEditor
