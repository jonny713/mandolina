import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/lore',
    name: 'Lore',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Lore.vue')
  },{
  //   path: '/economic-simulator',
  //   name: 'Economic simulator',
  //   component: () => import('../views/Economic-simulator.vue')
  // },{
    path: '/action-simulator',
    name: 'Action simulator',
    component: () => import('../views/Game.vue')
  },{
    path: '/sprite-map-editor',
    name: 'Sprite map editor',
    component: () => import('../views/Sprite-map-editor.vue')
  },{
    path: '/arena-editor',
    name: 'Arena editor',
    component: () => import('../views/Arena-editor.vue')
  },{
    path: '/ws-tester',
    name: 'ONE BUTTON GAME',
    component: () => import('../views/WebSocket-tester.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})
console.log(process.env.BASE_URL);
export default router
