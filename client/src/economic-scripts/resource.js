
/**
  Класс ресурса. Экземляр класса содержит информацию о самом ресурсе
*/
export default class Resource {
  constructor(name) {
    this.name = name
  }
}
