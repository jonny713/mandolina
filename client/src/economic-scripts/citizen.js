
/**
  Класс жителя. Экземпляр класса имеет место update, который возвращает иныу о потребленных ресурсах
*/
export default class Citizen {
  constructor(status, profession) {
    this.status = status
    this.profession = profession
  }
  update() {
    const result = [0, 0, 0]
    switch (this.status) {
      case 0: {
        result[1] -= 2
        break
      }
      case 1: {
        result[0] -= 1
        result[1] -= 1
        break
      }
      case 2: {
        result[0] -= 2
        break
      }
    }

    switch (this.profession) {
      case 0: {
        result[0] += 3
        result[2] -= 1
        break
      }
      case 1: {
        result[2] += 2
        break
      }
      case 2: {
        result[1] += 3
        result[2] -= 1
        break
      }
    }

    return result
  }
}
