import Citizen from './citizen'

/**
  Класс города. Экземпляр класса имеет метод update, который вызывает такие же методы у всех жителей
*/
export default class City {
  constructor(name, resourcePositions) {
    this.name = name
    this.citizens = []
    this.resourcePositions = resourcePositions
  }
  addCitizens(citizens) {
    this.citizens.push(...citizens)
  }
  update() {
    for (let citizen of this.citizens) {
      let result = citizen.update()
      this.resourcePositions[0].count += result[0]
      this.resourcePositions[1].count += result[1]
      this.resourcePositions[2].count += result[2]
    }

    if (this.resourcePositions[1].count > 550) {
      this.resourcePositions[1].count -= 50
      this.addCitizens([new Citizen(Math.floor(Math.random() * 3), Math.floor(Math.random() * 3))])
    }
  }

}
