import City from './city'
import Citizen from './citizen'
import Resource from './resource'
// import Fabric from './fabric'


const resources = ['Одежда', 'Еда', 'Материал'].map(r => new Resource(r))
const cities = [{
  name: 'Москва',
  resourcePositions: [{
    type: resources[0],
    count: 2500
  },{
    type: resources[1],
    count: 1200
  },{
    type: resources[2],
    count: 3000
  }]
},{
  name: 'Подольск',
  resourcePositions: [{
    type: resources[0],
    count: 4000
  },{
    type: resources[1],
    count: 3300
  },{
    type: resources[2],
    count: 1000
  }]

}]
  .map(c => {
    let city = new City(c.name, c.resourcePositions)

    let citizens = new Array(Math.floor(Math.random() * 5) + 15).fill()
    citizens = citizens.map(() =>
      new Citizen(Math.floor(Math.random() * 3), Math.floor(Math.random() * 3))
    )
    city.addCitizens(citizens)

    return city
  })

// console.log(cities)

// function createResourcesEl() {
//   const resourcesEl = document.getElementById('resources')
//   const ul = document.createElement("ul")
//
//   for (let resource of resources) {
//     const li = document.createElement("li")
//     li.textContent = resource.name
//     ul.appendChild(li)
//   }
//   resourcesEl.appendChild(ul)
// }
function createCitiesEl() {
  const citiesEl = document.getElementById('cities')
  citiesEl.innerHTML = ''
  const p = document.createElement("p")
  p.textContent = 'Города'
  const ul = document.createElement("ul")

  for (let city of cities) {
    const li = document.createElement("li")
    li.textContent = city.name + ', жителей ' + city.citizens.length
    const ul2 = document.createElement("ul")

    for (let resourcePosition of city.resourcePositions) {
      const li2 = document.createElement("li")
      li2.textContent = resourcePosition.type.name + ': ' + resourcePosition.count
      ul2.appendChild(li2)
    }

    li.appendChild(ul2)

    ul.appendChild(li)
  }
  citiesEl.appendChild(p)
  citiesEl.appendChild(ul)
}
function main() {
  for (let city of cities) {
    city.update()
    createCitiesEl()
  }

}

// createResourcesEl()
createCitiesEl()

export default class EconomicSimulator {
  constructor() {

  }
  addCity(name = 'name', resources = []) {
    this.cities.push(new City(name, resources))

    let citizens = new Array(Math.floor(Math.random() * 5) + 15).fill()
    citizens = citizens.map(() =>
      new Citizen(Math.floor(Math.random() * 3), Math.floor(Math.random() * 3))
    )
    city.addCitizens(citizens)

    return city
  }
}
() => {
  setInterval(main, 1000)

}
