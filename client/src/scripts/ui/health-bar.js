import UI from './ui-basic.js'
import { GeometryFrame } from './../draw/frame/geometry-frame'


export default class HealthBar extends UI {
	constructor(startPos, relatedElement) {
		super(startPos, relatedElement)

		this.frame = new GeometryFrame(this)

		this.width = 1.5
		this.size = 25
		this.health = 1

		// коэфициент центрования
		let k = (this.width - 1) / 2 * this.size
		this.offset.x -= k
	}

	set health(percent) {
		percent = this.width * Math.max(percent, 0)
		// console.log(percent);


		this.frame.reset()
		this.frame.addRect({ x: 0, y: 0 },{ x: this.width, y: .2 }, 'red')
		this.frame.addRect({ x: 0, y: 0 },{ x: this.width, y: .2 })
		this.frame.addRect({ x: 0, y: 0 },{ x: percent, y: .2 }, 'green')
	}
}
