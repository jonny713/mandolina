import StyledBlock from '../base-classes/styled-block'

export default class UI extends StyledBlock {
	constructor(startPos, relatedElement) {
		let offset = {
			x: startPos.x,
			y: startPos.y
		}

		startPos.x += relatedElement.position.x
		startPos.y += relatedElement.position.y

		super(startPos)

		this.drawingLayer = 3

		this.crossable = false
		this.relatedElement = relatedElement
		this.offset = offset
	}

	tic() {
		if (this.relatedElement.deleted) {
			this.desctruct()
		}

		this.position = {
			x: this.relatedElement.position.x + this.offset.x,
			y: this.relatedElement.position.y + this.offset.y,
		}

		return super.tic()
	}
}
