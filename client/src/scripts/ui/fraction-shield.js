import UI from './ui-basic.js'
import { GeometryFrame } from './../draw/frame/geometry-frame'

export default class FractionShield extends UI {
	constructor(startPos, relatedElement, color) {
		super(startPos, relatedElement)

		this.frame = new GeometryFrame(this)
		
		this.size = 25
		this.frame.addRect({ x: 0, y: 0 },{ x: this.width, y: .2 }, color)
		this.frame.addRect({ x: 0, y: 0 },{ x: this.width, y: .2 })
	}
}
