import UI from './ui-basic.js'
import { TextFrame } from './../draw/frame/text-frame'

export default class Text extends UI {
	constructor(startPos, relatedElement, text) {
		super(startPos, relatedElement)

		this.font = startPos.font
		this.text = text

		this.frame = new TextFrame(this)
	}

	set text(text) {
		this._text = this.frame.text = text
	}
	get text() {
		return this._text
	}

	set font(font) {
		this._font = this.size = font
	}
	get font() {
		return this._font
	}

	update(startPos, text) {
		this.font = this.size = startPos.font
		this.text = this.frame.text = text
	}
}
