import StyledBlock from './../base-classes/styled-block'
import { BaseFrame } from './../draw/frame/base'
import { spawn } from './../draw/spawner.js'
import Text from './text.js'

export default class HUD extends StyledBlock {
	constructor({ room, wave }) {
		super({ x: 0, y: 0 })
		this.frame = new BaseFrame(this)

		// this.elements = []
		this.HUDsize = {
			width: 1000,
			height: 1000
		}

		// Номер комнаты
		this._room = spawn(Text, { x: 0, y: 0, font: 32 }, this, `Room #${room}`)
		// Инфа о волне
		this._wave = spawn(Text, { x: 0, y: 0, font: 32 }, this, `Wave #${wave}`)

		// Здоровье
		// Нагрев
	}
	set room(room) {
		this._room.text = `Room #${room}`
	}
	set wave(wave) {
		this._wave.text = `Wave #${wave}`
	}

	resize({ width, height }) {
		this.HUDsize = {
			width,
			height
		}

		this.resizeElements()
	}
	resizeElements() {
		// Волшебный коэффициент.
		// TODO: Если будет больше элементов нужно сделать методы вычисления относительно права/лева верха/низа
		let k = 0.625
		this._room.offset.x = k * (this.HUDsize.width / 2) - 175
		this._room.offset.y = - k * (this.HUDsize.height / 2) + 70

		this._wave.offset.x = k * (this.HUDsize.width / 2) - 175
		this._wave.offset.y = - k * (this.HUDsize.height / 2) + 105
	}

	upd(position) {
		this.position = { ...position }
	}
}
