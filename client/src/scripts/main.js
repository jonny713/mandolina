/**
 *
 *
 */

import Performance from './draw/performance.js'
import ViewPort from './draw/view-port'
import initGame from './game'
import vueConnector from './game/vue-connector'
import Mouse from './inputs/mouse.js'
import PlayCharacter from './characters/play-character.js'
import Camera from './camera.js'
import HUD from './ui/hud.js'
// import webglMain from './webgl/main.js'
import StyledBlock from './base-classes/styled-block'
import { defaultTileMap } from './draw/tiles/animation-tile-map'



import Keys from './inputs/keys.js'
import Level from './level/level.js'

import {
	spawn,
	assignGlobalSpawnMethodToViewPort,
	assignGlobalSpawnMethodToGame,
	updateSpawn
} from './draw/spawner.js'


function startGame({ fpsMeter, canvas, webgl, gameData }) {
	// Подключаем счетчик FPS
	vueConnector.addFpsMeter(fpsMeter)

	// Создаем счетчик кадров и устанавливаем частоту кадров
	let perf = Performance.counter()
	perf.fps = 90

	// Создаем вью порт
	const viewPort = new ViewPort(canvas)
	// console.log('Создан viewPort', viewPort);
	updateSpawn((...args) => viewPort.spawn(...args))

	// Создаем сетевую ИГРУ
	const game = initGame(gameData)
	game.assignViewPort(viewPort)

	// Создаем уровень по сохраненному имени предподготовленного уровня
	const level = new Level({ levelName: 'arena', offset: { x: -690, y: -550 } })
	// Прикрепляем уровень
	viewPort.assignLevel(level)

	// Прикрепляем webGL
	// viewPort.assignWebGL(webglMain(webgl))

	// Добавляем слушатель мыши
	let mouse = viewPort.assignMouse(Mouse)

	// Добавляем игрока
	let player = viewPort.spawn(PlayCharacter, {x: 265, y: 125, fraction: 'player', gamerName: gameData.gamer})
	player.assignMouse(mouse)

	// Добавляем камеру
	let camera = viewPort.assignCamera(Camera, player)

	// Создаем HUD
	let hud = viewPort.spawn(HUD, {})
	viewPort.assignHUD(hud)
	game.assignHUD(hud)

	viewPort.spawn(StyledBlock, {x: 265, y: 125, size:5, animationTileMap: defaultTileMap, direction: Math.PI/2 })
	// viewPort.spawn(StyledBlock, {x: 265, y: 125, image: defaultTileMap.getTile(0), direction: Math.PI/4 })
	// viewPort.spawn(StyledBlock, {x: 265, y: 125, image: defaultTileMap.getTile(0), direction: 3*Math.PI/4 })


	game.start()

	// Отправляем наружу коннектор
	return vueConnector
}

export default startGame
