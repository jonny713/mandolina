
export class ModeSelection {
	mods = {}
	actionMode = null
	constructor() {}

	/**
	 * @param {Mode} mode
	 */
	add({ name, frames, onTic, onStart, onEnd }) {
		this.mods[name] = new Mode({ name, frames, onTic, onStart, onEnd })
	}

	/**
	 * Стартует указанный режим
	 */
	select(name, ...args) {
		this.actionMode = this.mods[name]
		this.mods[name].start(...args)
	}

	get activeModeName() {
		if (this.actionMode) {
			return this.actionMode.name
		} else {
			return 'unknown'
		}
	}

	tic(nearObjects) {
		// Если запущен какой-то режим, то отдаем обработку туда
		if (this.actionMode && this.actionMode.frames) {
			return this.actionMode.tic(nearObjects)
		}

		// Пробуем выбрать режим
		for (let name in this.mods) {
			this.select(name)
			return
		}
	}
}

export class Mode {
	// Создает новый экземпляр, запоминает
	constructor({ name, frames, onTic, onStart, onEnd }) {
		this.name = name
		this.frames = 0
		this.maxFrames = frames
		this.onTic = onTic
		this.onStart = onStart
		this.onEnd = onEnd
	}

	start(...args) {
		// console.log(`ЗАПУЩЕН РЕЖИМ ${this.name}`);
		this.frames = this.maxFrames
		if (this.onStart) {
			this.onStart(...args)
		}
	}

	tic(nearObjects) {
		this.frames--
		if (this.onTic) {
			this.onTic(nearObjects)
		}
		if (!this.frames) {
			if (this.onEnd) {
				this.onEnd(nearObjects)
			}
		}
	}
}
