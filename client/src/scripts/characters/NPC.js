import Character from './character.js'
import FractionShield from './../ui/fraction-shield.js'
// import Ray from './../ray.js'
// import { monstersFraction } from './fraction.js'
import { spawn } from './../draw/spawner.js'
import { ModeSelection } from './ai/mode.js'

import BadBigGun from './../guns/bad-big-gun.js'
import BadBigAmmo from './../bullets/bad-big-ammo.js'


export default class NPC extends Character {
  calculateDistanceToTarget(target) {
		if (!target) {
			return 9999
		}
    return Math.sqrt((this.position.x - target.position.x) ** 2 + (this.position.y - target.position.y) ** 2)
  }

  constructor(initParam, /*relatedElement*/) {
    super(initParam)

    this.attackDistance = 120
    this.lookDistance = 7000

    // this._speed.max = 15

    this.fractionShield = spawn(FractionShield, { x: -7, y: 0 }, this, this.fraction.color)

		this.searchEnemy()
		this.modeSelectioner = new ModeSelection()
		this.modeSelectioner.add({
			name: 'idle',
			frames: 20,
			onEnd: () => {
				let enemy = this.focused.target

				// считаем дистанцию до него
				let distance = this.calculateDistanceToTarget(enemy)

				// Проверяем дистанцию до цели и выбираем соответсвующий режим
				if (distance < this.attackDistance) {
					// Если цель близко, включаем атаку
					this.modeSelectioner.select('attack')
				} else {
					this.modeSelectioner.select('follow')
				}
			},
		})
		this.modeSelectioner.add({
			name: 'attack',
			frames: 20,
			onStart: () => this.moveTo(this.position, 20),
			onTic: () => {
				let enemy = this.focused.target

				// Целимся
				this.lookTo(enemy?.position)

				// Стреляем
				this.useActiveItem()
			},
			onEnd: () => this.modeSelectioner.select('idle'),
		})
		this.modeSelectioner.add({
			name: 'follow',
			frames: 50,
			onStart: () => {
				let enemy = this.focused.target

				// Следуем за преследуемым
				// this.moveTo(enemy?.position)
				this.moveTo({
					x: enemy.position.x - Math.sin(this.direction) * this.globalSize * 2,
					y: enemy.position.y - Math.cos(this.direction) * this.globalSize * 2,
				})

			},
			onTic: () => {
				let enemy = this.focused.target

				// считаем дистанцию до него
				let distance = this.calculateDistanceToTarget(enemy)

				// Проверяем дистанцию до цели и выбираем соответсвующий режим
				if (distance < this.attackDistance) {
					// Если цель близко, включаем атаку
					// this.modeSelectioner.select('attack')
					// Целимся
					this.lookTo(enemy?.position)

					// Стреляем
					this.useActiveItem()
				}
			},
			onEnd: () => this.modeSelectioner.select('idle'),
		})
	}

	/**
	*  NPC осматривается вокруг в поисках цели
	*
	*  @returns {Object} enemy враг
	*/
	lookArround() {
		for (let enemy of this.fraction.enemies) {
			// Отметаем тех, кто слишком далеко
			if (Math.abs(this.position.x - enemy.position.x) > this.lookDistance || Math.abs(this.position.y - enemy.position.y) > this.lookDistance) {
				continue
			}

			// Проверяем, что противник виден
			if (this.cell.isOverview(this.position, enemy.position)) {
				this.focused = {
					target: enemy,
					aggro: 1,
				}
				return enemy
			}
		}
	}
	searchEnemy(priorityTarget) {
		if (priorityTarget) {
			this.focused = {
				target: priorityTarget,
				aggro: 1,
			}
		} else {
			this.focused = {
				target: this.fraction.enemies[0],
				aggro: 1,
			}
		}
	}

	useActiveItem() {
		super.useActiveItem()

		// this.push({
		// 	x: -Math.cos(this.direction) * this.globalSize * 3/5,
		// 	y: Math.sin(this.direction) * this.globalSize * 3/5,
		// })
	}


  tic(nearObjects = []) {
		// this.modeSelection()
		this.modeSelectioner.tic(nearObjects)

    this.isAttacked = this.modeSelectioner.activeModeName === 'attack'
    return super.tic(nearObjects)
  }

	aggroList = {}
	takeDamage({ attack, aggroModifier = 1 }, owner) {
		super.takeDamage({ attack, aggroModifier }, owner)
		// Добавляем атаковавшего в список противников
		if (!this.aggroList[owner.id]) {
			this.aggroList[owner.id] = {
				target: owner,
				aggro: 0,
			}
		}

		// Проверяем, является ли противник более приоритетной целью
		this.aggroList[owner.id].aggro += attack * aggroModifier
		if (this.aggroList[owner.id].aggro > this.focused.aggro) {
			// this.focused = { ...this.aggroList[owner.id].target }
			this.focused = this.aggroList[owner.id]
		}
	}
	_focused = {
		target: { position: { x: 0, y: 0 } },
		aggro: 0
	}
	get focused() {
		return this._focused
	}
	set focused({ target, aggro }) {
		this._focused.target = target
		this._focused.aggro = aggro
	}
}
