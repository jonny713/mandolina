import { AnimationFrame } from './../draw/frame/animation-frame'
import FractionBlock from './../fraction/fraction-block.js'
import FractionShield from './../ui/fraction-shield.js'
import Text from './../ui/text.js'
import Storage from '../storages/storage.js'
import { spawn } from './../draw/spawner.js'

import gunByName from './../guns'
import bulletByName from './../bullets'
import Bullet from './../bullets/bullet.js'

import HealthBar from '../ui/health-bar.js'

// TODO: Внутри есть куча функционала, нужного только для сетевой игры - нужно вынести
export default class Character extends FractionBlock {
	isPushable = true

	constructor(...args) {
		super(...args)
		this.name = args[0].gamerName || 'noname'
		this.maxWeight = 100
		this.size = 23
		this._speed.max = 25

		// Инвентарь
		this.inventory = new Proxy(new Storage(), {
			get: (target, prop, reciever) => {
				if (prop !== 'saveItem' && prop !== 'getItem' && prop !== 'clear') return target[prop]
				return (...args) => {
					const result = target[prop].apply(reciever, args)
					const weight = target.weight
					// console.log(prop, weight)
					if (weight > this.maxWeight && !this.overWeight) {
						this.overWeight = true
						// console.log('ПЕРЕВЕС')

					}
					if (weight < this.maxWeight && this.overWeight) {
						this.overWeight = false
						// console.log('Перевес устранен')

					}
					return result
				}
			}
		})


		// Здоровье персонажа
		this.healthMax = args[0].healthMax || 100
		this.isPushable = args[0].isPushable !== undefined ? args[0].isPushable : this.isPushable
		this.isAlive = true
		this.healthBar = spawn(HealthBar, { x: -10, y: -20 }, this)
		this.fractionShield = spawn(FractionShield, { x: -7, y: 0 }, this, this.fraction.color)

		// TODO: бред по имени это делать
		this.setActiveItem('big')

		this.frame = new AnimationFrame(this, args[0].animationTileMap)
	}

	/**
	* @summary Инфа о занимаемых ячейках
	*/
	get cells() {
		let cell = this.cell
		return [
			cell,									cell.left,
			cell.down,						cell.left.down,
			cell.down.down,				cell.left.down.down,
		]
	}

	/**
	* @summary Метод обновления имени, нужен для сетевой игры
	*/
	updateName() {
		if (!this.nameText) {
			this.nameText = spawn(Text, { x: 0, y: -this.size, font: this.size }, this, this.name)
			return
		}

		this.nameText.font = this.size
		this.nameText.text = this.name
	}

	setActiveItem(itemName) {
		let Gun = gunByName(itemName)
		let Bullet = bulletByName(itemName)

		// TODO: Надо сделать пульки бесконечными
		// Даем на тест базовое кол-во выстрелов
		this.inventory.saveItems(new Bullet(), 9999)

		// Устанавливаем в качесвте активного предмета экземпляр пушки
		this.activeItem = new Gun(this)

		this.activeItemName = itemName
	}

	/**
	* @summary Метод жесткой смены здоровья, нужен для сетевой игры
	*/
	healthTo(val) {
		val = val > this.healthMax ? this.healthMax : val
		this.takeDamage(this.health - val)
	}
	get health() {
		if (!('_health' in this))
		this._health = this.healthMax
		return this._health
	}

	set health(val) {
		if (!('_health' in this))
		this._health = this.healthMax
		return this._health = val
	}

	get isAlive() {
		return !!this._isAlive
	}
	set isAlive(val) {
		return this._isAlive = !!val
	}

	useActiveItem(position) {
		// TODO: Нет смысла передавать позицию и прочее КАЖДЫЙ раз
		this.activeItem.attack({
			reloadMethod: (type, count) => {
				// console.log(this.inventory.list[0])
				// console.log(this.inventory.list[1])
				// console.log(this.inventory.list[2])
				// console.log(this.inventory.list[7])

				let bullets = this.inventory.getItems({ type }, count)
				// console.log(bullets);
				return bullets.length
			},
			position: position || this.moveToPosition || this.position,
			direction: this.direction,
			fraction: this.fraction
		})
	}


	death() {
		// Помечаем персонажа мертвым
		this.isAlive = false

		// Порождаем лут

		// Убираем данного персонажа
		this.desctruct()

	}
	calculatePosition() {
		super.calculatePosition()
		for (let near of this.fraction.members) {
			if (near instanceof Bullet) {
				continue
			}
			if (this.cell.compareCellWithZone(near.cell)) {
				this.crossTrigger(near)
			}
		}
	}
	// Срабатываемое событие пересечения
	crossTrigger(crossObject) {
		// Если столкнулись с представителем своей фракции, то отталкиваем
		if (crossObject?.fraction && crossObject.fraction == this.fraction && this.isPushable) {
			let x = //this.position.x +
			Math.sign(this.position.x - crossObject.position.x) * this.size
			let y = //this.position.y +
			Math.sign(this.position.y - crossObject.position.y) * this.size

			this.push({ x, y })
		}
	}

	// Получение урона извне
	takeDamage({ attack: damage }) {
		if (typeof damage == 'number' && !this.invincibilityFrames) {
			this.health -= damage

			this.healthBar.health = this.health / this.healthMax

			this.invincibilityFrames = 30

			if (this.health <= 0) {
				this.death()
			}
		}
	}

	tic(nearObjects) {
		if (this.invincibilityFrames > 0) {
			this.invincibilityFrames--
		}
		return super.tic(nearObjects)
	}
}
