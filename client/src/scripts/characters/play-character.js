/** @module characters/play-character.js */

import Character from './character.js'
import Keys from './../inputs/keys.js'

// import { playerFraction } from './characters/fraction.js'
// import Bullet from './bullets/bullet.js'

import Gun from './../guns/gun.js'
import Ammo from './../bullets/ammo.js'

const MAX_SPEED = 15

/**
 * Персонаж с отслеживанием управления
 * @class
 * @extends Character
 */
export default class PlayCharacter extends Character {
  /**
   * Создание нового игрового персонажа
   * @param {object} start - объект с параметрами инициализации рисуемого объекта
   * @param {string} inputType - тип отслеживания направления - enum:direct,mouse
   * @param {Mouse} mouseObject - объект, отслеживающий состояние мыши
   */
  constructor(start) {
    super(start)
    console.log(`Во фракцию ${this.fraction.name} добавлен PlayCharacter с id ${this.id}`);


    this.keys = {
      up: Keys.associate('KeyW'),
      down: Keys.associate('KeyS'),
      left: Keys.associate('KeyA'),
      right: Keys.associate('KeyD'),
      space: Keys.associate('Space'),
      special: Keys.associate('KeyQ'),
      heal: Keys.associate('KeyH'),
    }

		this.healthMax = 500000
		this.size = 23
		this._speed.max = MAX_SPEED

		this.frames.charge = 0

		this.setActiveItem('classic')
    return this
  }

	charge() {
		if (!this.frames.charge) {
			console.log('CHARGE');
			this.frames.charge = 120
			this._speed.max = 100
			// this.push({
			// 	x: Math.cos(this.direction) * this.globalSize * 10,
			// 	y: -Math.sin(this.direction) * this.globalSize * 10,
			// })
		}
	}

  assignMouse(mouseObject) {
    this.mouseObject = mouseObject
  }

  calculateForces() {
		if (this._speed.max > MAX_SPEED) {
			this._speed.max -= 3
		}
    return {
      x: +this.keys.right.pressed - +this.keys.left.pressed,
      y: +this.keys.down.pressed - +this.keys.up.pressed,
    }
  }

  calculateDirection() {
    if (!this.mouseObject.inWorkSpace)
      return this._direction

    this._direction = Math.atan2(this.mouseObject.relativePosition.x - this.position.x, this.mouseObject.relativePosition.y - this.position.y) - Math.PI/2
    return this._direction
  }

  useActiveItem(position) {
		super.useActiveItem(this.position)
	}

	calculateInputs() {
		// this.keys.up.value = parseInt(this.speed.y < 0 ? -this.speed.y : 0)
		// this.keys.down.value = parseInt(this.speed.y > 0 ? this.speed.y : 0)
		//
		// this.keys.left.value = parseInt(this.speed.x < 0 ? -this.speed.x : 0)
		// this.keys.right.value = parseInt(this.speed.x > 0 ? this.speed.x : 0)

		if (this.keys.space.pressed) {
			this.charge()
		}
		// this.keys.space.value = parseInt(this.frames.charge)
	}

	frames = {}
	calculateFrames() {
		for (let f in this.frames) {
			this.frames[f] = this.frames[f] > 0 ? this.frames[f] - 1 : 0

		}
	}
	adminHeal() {
		this.health = this.healthMax
	}

  tic(nearObjects) {
		this.calculateInputs()
		this.calculateFrames()

    if (this.mouseObject.mouseDown) {
      this.useActiveItem()
    }
		if (this.keys.heal.pressed) {
      this.adminHeal()
    }
		this.isAttacked = !!this.mouseObject.mouseDown

    return super.tic(nearObjects)
  }

	crossTrigger(/*crossObject*/) {
		// this.health = this.health || 100500
		// this.health -= crossObject.attack
		// console.log(this.health)
	}
}
