// let count = 7586854
// let uuid = () => count++

export default class Storage {
  constructor(size = 20, stackSize = 9999) {
    this.cells = new Array(size).fill().map((c, i) => ({
      index: i,
      empty: true,
      type: null,
      count: 0,

    }))

    this.weight = 0

    this._list = {}
    this.types = {}
    this._stackSize = stackSize
  }

  // // Подсчет веса хранилища
  // get weight() {
  //   return this._weight
  // }
  // set weight(newWeight = 0) {
  //   this._weight = newWeight
  //   return this._weight
  // }

  // Получить предмет
  getItem({ id, type }) {
    let index

    if (type) {
      if (!this.types[type.name])
        return

      index = this.types[type.name][0]
    } else {
      if (typeof id !== 'number')
        return

      if (!this.cells[id])
        return

      index = id
    }

    let item = this.cells[index].item.stackable ? new this.cells[index].type() : this.cells[index].item
    this.cells[index].count--

    if (this.cells[index].count <= 0) {
      this.types[this.cells[index].type.name] = this.types[this.cells[index].type.name].filter(t => t !== index)
      if (this.types[this.cells[index].type.name].length === 0) {
        delete this.types[this.cells[index].type.name]
      }

      this.cells[index].empty = true
      this.cells[index].type = null
      delete this.cells[index].item
    }

    this.weight -= item.weight
    return item
  }

  // Получить несколько предметов
  getItems({ id, type }, count) {
    let items = []
    for (let i = 0; i < count; i++) {
      let item = this.getItem({ id, type })
      if (!item)
        return items

      items.push(item)
    }
    return items
  }

  // Сохранить предмет
  saveItem(item) {
    if (!item || !item.storageble)
      return this.weight

    // ищем элемент того же класса
    if (item.stackable && this.types[item.constructor.name]) {
      for (let c of this.types[item.constructor.name]) {
        if (this.cells[c].count < this._stackSize) {
          this.cells[c].count++

          this.weight += item.weight
          return this.weight
        }
      }
    }

    // Выбираем пустую ячейку
    let index = this.cells.findIndex(c => c.empty)

    // Действия, если свободных ячеек нет
    if (!~index) {
      return this.weight
    }

    // Если ячейка найдена
    this.cells[index].id = index
    this.cells[index].empty = false
    this.cells[index].type = item.constructor
    this.cells[index].item = item
    this.cells[index].count = 1

    if (!this.types[item.constructor.name])
      this.types[item.constructor.name] = []

    this.types[item.constructor.name].push(index)
    // console.log(this.types);

    this.weight += item.weight
    return this.weight
  }

  // Сохранить несколько однотипных предметов
  saveItems(item, count) {
    for (let i = 0; i < count; i++) {
      this.saveItem(item)
    }
  }

  //
  replace(idFrom, idTo) {
    if (typeof idFrom !== 'number' || typeof idTo !== 'number')
      return

    if (!this.cells[idFrom] || !this.cells[idTo])
      return

    this.cells[idFrom].index = idTo
    this.cells[idTo].index = idFrom

    let typeFromName = this.cells[idFrom].type ? this.cells[idFrom].type.name : null
    let typeToName = this.cells[idTo].type ? this.cells[idTo].type.name : null

    if (typeFromName)
      this.types[typeFromName][this.types[typeFromName].findIndex(i => i == idFrom)] = idTo

    if (typeToName)
      this.types[typeToName][this.types[typeToName].findIndex(i => i == idTo)] = idFrom

    this.cells.sort((a, b) => Math.sign(a.index - b.index))
  }

  // Получить список предметов
  get list() {
    return this.cells
  }

	// Опустошает инвентарь
	clear() {
		
	}

}
