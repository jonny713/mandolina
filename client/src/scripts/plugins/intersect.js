

export default class Intersect {
  constructor(crossable, center, size) {

    this.isCrossable = crossable
    this.isTransparent = false
    this.center = center
    this.size = size
    this.points = []


  }

  get isCrossable() {
    return this._crossable
  }
  set isCrossable(val) {
    this._crossable = !!val
    return this._crossable
  }

  get size() {
    return this._size
  }
  set size(val) {
    if (val !== this._size) {
      this._size = val
      this.innerRadiusSquare = this._size ** 2
      this.outerRadiusSquare = this.innerRadiusSquare * 2
    }
    return this._size
  }

  /*
    Обновлем точки
  */
  upgradePoints(center, size, points) {
    this.center = center
    this.size = size
    this.points = points
  }

  /*
    Проверяет, пересекает ли указанная точка отслеживаемый объект
  */
  withPoint(pos) {
    let { center, outerRadiusSquare, innerRadiusSquare } = this

    let distanceSquare =  (pos.x - center.x)**2 + (pos.y - center.y)**2
    // Если точка pos не входит в описанную вокруг объекта окружность, значит пересечения  точно нет
    if (distanceSquare > outerRadiusSquare)
      return false

    // Если точка pos входит во вписанную окружность, значит пересечения точно есть
    if (distanceSquare < innerRadiusSquare)
      return true

    //
  }

  /**
  * @param {Vector} from источник луча
  * @param {Vector} to
  *
  */
  withRay(from, to) {
    if (this.isTransparent)
      return false

    let { center, outerRadiusSquare, innerRadiusSquare } = this

    // Квадрат расстояния между источником и объектом
    let aSquare = (from.x - center.x) ** 2 + (from.y - center.y) ** 2

    // Квадрат расстояния между целью и объектом
    let bSquare = (to.x - center.x) ** 2 + (to.y - center.y) ** 2

    // Квадрат расстояния между целью и объектом
    let cSquare = (to.x - from.x) ** 2 + (to.y - from.y) ** 2

    if (aSquare > cSquare || bSquare > cSquare || aSquare + bSquare > cSquare * 1.5  )
      return false

    // Уравнение прямой
    //  y = kx + b
    let k = (from.y - to.y) / (from.x - to.x)
    let b = (from.x * to.y - to.x * from.y) / (from.x - to.x)

    // Уравнение окружности
    // (x - center.x)**2 + (y - center.y)**2 = r**2

    // Решаем систему уравнений, получаем квадратное уравнение и его коэфициенты
    // (x - center.x) ** 2 + (k * x + b - center.y) ** 2 = r**2
    // x ** 2 - 2 * center.x * x + center.x ** 2 + (k * x) ** 2 + 2 * (b - center.y) * x + (b - center.y) ** 2 = r**2
    // (k**2 + 1) * (x**2) + (2 * (b - center.y) - 2 * center.x) * x + (center.x ** 2 + (b - center.y) ** 2 - r**2) = 0
    let _a = k**2 + 1
    let _b = 2 * k * (b - center.y) - 2 * center.x
    let _cOuter = center.x ** 2 + (b - center.y) ** 2 - outerRadiusSquare
    let _cInner = center.x ** 2 + (b - center.y) ** 2 - innerRadiusSquare

    // считаем дискриминант
    let discr = (a, b, c) => b * b - 4 * a * c;

    // Проверяем с описанной окружностью
    // Если дискриминант меньше 0, то пересечения нет
    if (discr(_a, _b, _cOuter) < 0)
      return false

    // Проверяем с вписанной окружностью
    // Если дискриминант блоьше либо равен 0, то пересечение есть
    if (discr(_a, _b, _cInner) >= 0)
      return true

    // Для всех остальных случаев пока что считаем, что пересечение есть
    return true
  }
}
