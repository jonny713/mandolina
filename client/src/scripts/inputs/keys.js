import vueConnector from '../game/vue-connector'


export default class Keys {
	_isPressed = false
	constructor(keyCode) {
		vueConnector.onPress(keyCode, isPressed => this._isPressed = isPressed)
		return this
	}

	get pressed() {
		return this._isPressed
	}

	static associate(keyCode) {
		return new Keys(keyCode)
	}
}
