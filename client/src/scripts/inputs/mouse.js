import StyledBlock from '../base-classes/styled-block'
import { MouseFrame } from './../draw/frame/mouse-frame'


/**
* Cлушатель мыши
*/
export default class Mouse extends StyledBlock {
	constructor(initialParameters) {
		super(initialParameters)
		this.size = 3
		this.frame = new MouseFrame(this)
		this.crossable = false

		this.inWorkSpace = true
		this.mouseDown = false

		this._wheel = {
			min: -105,
			max: -105,
			value: -105,
			speed: {
				min: 0,
				max: 15,
				dir: 1,
				moment: 0
			}
		}
		window.addEventListener('mousemove', e => {
			this.position = {
				x: e.x,
				y: e.y,
			}
		})
		window.addEventListener('mouseover', () => this.inWorkSpace = true)
		window.addEventListener('mouseout', () => this.inWorkSpace = false)

		window.addEventListener('mousedown', e => this.onMouseDown(e))
		window.addEventListener('mouseup', e => this.onMouseUp(e))

		// Добавляем обработку касаний на телефонах
		window.addEventListener("touchstart",  e => {
			if (e.changedTouches[0].pageX < 180 && window.screen.height - e.changedTouches[0].pageY < 180 ) {
				return
			}
			this.onMouseDown(e)
		}, false);
		window.addEventListener("touchend",  e => this.onMouseUp(e), false);
		window.addEventListener("touchcancel", e => this.onMouseUp(e), false);
		window.addEventListener("touchmove", e => {
			if (e.changedTouches[0].pageX < 180 && window.screen.height - e.changedTouches[0].pageY < 180 ) {
				return
			}
			this.position = {
				x: e.changedTouches[0].pageX,
				y: e.changedTouches[0].pageY,
			}
		}, false);

		window.addEventListener("wheel",  e => {
			if (e.deltaY > 0) {
				this.wheelSpeed += 5
			} else {
				this.wheelSpeed -= 5
			}
		})
	}

	get wheelSpeed() {
		return this._wheel.speed.dir * this._wheel.speed.moment
	}
	set wheelSpeed(value) {
		this._wheel.speed.dir = Math.sign(value)
		value = Math.abs(value)
		if (value < this._wheel.speed.min) {
			value = this._wheel.speed.min
		}
		if (value > this._wheel.speed.max) {
			value = this._wheel.speed.max
		}
		return this._wheel.speed.moment = value
	}
	get wheel() {
		return this._wheel.value
	}
	set wheel(value) {
		if (value < this._wheel.min) {
			value = this._wheel.min
		}
		if (value > this._wheel.max) {
			value = this._wheel.max
		}
		return this._wheel.value = value
	}

	onMouseDown() {
		this.mouseDown = true

	}

	onMouseUp() {
		this.mouseDown = false

	}

	get relativePosition() {
		return {
			x: this.position.x - this.offset.x,
			y: this.position.y - this.offset.y,
		}
	}

	offset = {
		x: 0,
		y: 0,
	}
	setOffset({ x, y }) {
		this.offset = { x, y }
	}

	calculateWheelSpeed() {
		// меняем значение прокрутки
		this.wheel += this.wheelSpeed

		// уменьшаем скорость прокрутки
		if (this.wheelSpeed > 0) {
			this.wheelSpeed--
		}
		if (this.wheelSpeed < 0) {
			this.wheelSpeed++
		}

	}

	tic(...args) {
		this.calculateWheelSpeed()

		return super.tic(...args)
	}
}
