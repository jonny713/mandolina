import { GeometryFrame } from './draw/frame/geometry-frame'

import MovableBlock from './base-classes/movable-block.js'

// Расчет расстояния между точками
let calcDistance = (point1, point2) =>
	((point2.x - point1.x) ** 2 + (point2.y - point1.y) ** 2) ** .5

export default class Camera extends MovableBlock {
  constructor(startPos, relatedElement) {
    super(relatedElement ? relatedElement.position : startPos)
    this.crossable = false

    this.relatedElement = relatedElement

    this._speed.a = .15
    this._speed.max = this.relatedElement._speed.max

    this.accuracy = 15

    // this._direction = Math.PI / 2
		this.size = 4

		this.frame = new GeometryFrame(this)
  }

  get visibles() {
    if (this._visibles) {
      return this._visibles
    }
    this._visibles = {}
    return this._visibles
  }

  calculateDrawPoints() {
    let _size = 2
    let pos1 = {
      x: this.position.x + Math.cos(this.direction)*_size,
      y: this.position.y - Math.sin(this.direction)*_size
    }, pos2 = {
      x: this.position.x + Math.cos(3*Math.PI/4 + this.direction)*_size,
      y: this.position.y - Math.sin(3*Math.PI/4 + this.direction)*_size
    }, pos3 = {
      x: this.position.x + Math.cos(5*Math.PI/4 + this.direction)*_size,
      y: this.position.y - Math.sin(5*Math.PI/4 + this.direction)*_size
    }
    return [pos1,pos2,pos3]
  }

  checkVisible(drawObject) {
    if (drawObject.relatedElement) {
      if (
          this.relatedElement !== drawObject.relatedElement &&
          drawObject.relatedElement.fraction &&
          !this.cell.isOverview(this.position, drawObject.relatedElement.position)
        ) {

          if (this.visibles[drawObject.id]) {
            this.visibles[drawObject.id]--
            return true
          }
          return false
        }
    } else {
      if (
          this.relatedElement !== drawObject &&
          drawObject.fraction &&
          !this.cell.isOverview(this.position, drawObject.position)
        ) {

          if (this.visibles[drawObject.id]) {
            this.visibles[drawObject.id]--
            return true
          }
          return false
        }
    }

    // кол-во кадров, в течение которых будут видны уже невидимые объекты
    this.visibles[drawObject.id] = 100
    return true
  }

  tic() {
		// Ускоряем камеру в зависимости от дальности
		let distance = calcDistance(this.position, this.relatedElement.position)
		this._speed.max = (distance / 50)**2 + this.relatedElement._speed.max
    this.moveTo(this.relatedElement.position, this.accuracy)
		this.direction += .01
    return super.tic()
  }
}
