import Master from './master'
import Subject from './subject'

export default ({ code, master, gamer, connection }) => {
	return master === gamer ?
		new Master({ code, master, gamer, connection }) :
		new Subject({ code, master, gamer, connection })
}
