import { Game, waitCondition, DELAY } from './base'
import NPC from '../characters/NPC.js'
import Character from '../characters/character.js'
import Bullet from '../bullets/bullet.js'
import { spawn } from '../draw/spawner.js'


export default class Master extends Game {
	assign(spawnedObject) {
		super.assign(spawnedObject)

		// Если NPC, добавляем к списку врагов
		if (spawnedObject instanceof NPC) {
			this.enemies[spawnedObject.id] = spawnedObject
			this.send(
				'newEnemy',
				spawnedObject,
			)
			return
		}

		// Если перс, то это прочие игроки
		if (spawnedObject instanceof Character) {
			this.gamers[spawnedObject.name] = spawnedObject
			return
		}

		// Если пуля, отправляем
		if (spawnedObject instanceof Bullet) {
			this.send(
				'newBullet',
				spawnedObject,
			)
			return
		}
	}

	start() {
		super.start()
		this.startLobbi()
	}

	/**
	* @summary Стартует лобби
	*/
	startLobbi(complexity = 0) {
		// Всякие действия
		//
		// Дожидаемся действия игрока
		//
		this.startWave(complexity + 1)
	}
	/**
	* @summary Начинает новую волну
	*/
	async startWave(complexity) {
		this.hud.wave = complexity

		// Обнуляем
		this.enemies = {}
		// Генерим противников
		for (let i = 1; i <= 0 + Math.round(complexity * .7); i++) {
			for (let j = 1; j <= 0 + Math.round(complexity * .4); j++) {
				spawn(NPC, {x: j*(52) - 50, y: i*(52) - 50, fraction: 'monsters' })
			}
		}

		// Дожидаемся успешного завершения уровня
		let isWin = true
		await waitCondition(() => {
			// Если у всех игроков статус deleted, то поражение
			if (Object.values(this.gamers).every(gamer => gamer.deleted)) {
				isWin = false;
				return true;
			}
			// Если у всех противников статус deleted, то победа
			if (Object.values(this.enemies).every(enemy => enemy.deleted)) {
				return true;
			}
		})

		if (isWin) {
			console.log(`Волна ${complexity}, ПОБЕДА`);
			this.startLobbi(complexity)
		} else {
			console.log(`Волна ${complexity}, поражение`);


		}
	}

	tic() {
		super.tic()

		// Обновлем противников
		this.updateEnemies()
	}

	// Для противников пропускаем 20 кадров
	delayEnemies = DELAY * 2
	updateEnemies() {
		if (this.delayEnemies) {
			this.delayEnemies--
			return
		}
		this.delayEnemies = DELAY * 2

		for (let e in this.enemies) {
			let enemy = this.enemies[e]

			if (enemy.deleted) {
				this.send('deleteEnemy', enemy)
				delete this.enemies[e]
				continue;
			}
			this.send('moveEnemy', enemy)
		}
	}
}
