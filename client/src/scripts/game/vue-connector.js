export default new (class VueConnector {
	constructor() {

	}

	_fpsMeter
	get fpsMeter() {
		if (this._fpsMeter) {
			return this._fpsMeter
		} else {
			console.log('NO FPS METER')
			return null
		}
	}
	addFpsMeter(fpsMeter) {
		this._fpsMeter = fpsMeter
	}

	onPressHandlers = []
	press({ code, isPressed }) {
		if (this.onPressHandlers[code]) {
			this.onPressHandlers[code](isPressed)
		}
	}
	onPress(code, handler) {
		this.onPressHandlers[code] = handler
	}
})
