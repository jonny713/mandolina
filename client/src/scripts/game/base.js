import PlayCharacter from '../characters/play-character.js'
// import NPC from '../characters/NPC.js'
import Character from '../characters/character.js'
// import Bullet from '../bullets/bullet.js'
import { spawn } from '../draw/spawner.js'


export const waitCondition = async (condition, ts = 1000) => {
	if (!condition()) {
		await new Promise(resolve => setTimeout(resolve, ts))
		return waitCondition(condition, ts)
	}
}
export const DELAY = 3
export const ACCURACY = 7.5
export const INIT_ACTIONS = ['start', 'newEnemy']


export class Game {
	isMaster = false
	player = null
	gamers = {}
	enemies = {}
	send(action, spawnedObject) {
		let params = {
			action,
			id: spawnedObject.id,
			coords: spawnedObject.position,
			health: spawnedObject.health,
			direction: spawnedObject.direction,
			activeItemName: spawnedObject.activeItemName,
			isAttacked: !!spawnedObject.isAttacked,
			focusedPos: spawnedObject.focused ? spawnedObject.focused.position : null,
		}
		if (INIT_ACTIONS.includes(action)) {
			params.fraction = spawnedObject.fraction.name
			params.healthMax = spawnedObject.healthMax
		}
		this.connection.send(params)
	}
	constructor({ code, master, gamer, connection }) {
		this.connection = connection
		this.masterName = master
		this.gamerName = gamer
		this.isMaster = master === gamer
		this.code = code

		this.connection.onMessage((data) => this.onMessage(data))
	}

	onMessage(data) {
		this.onGamersUpdate(data.gamers)
		this.onEnemiesUpdate(data.enemies)
	}
	onGamersUpdate(gamers) {
		for (let gamer of gamers) {
			if (!gamer.active) {
				continue;
			}
			if (gamer.name === this.gamerName) {
				continue;
			}
			// Проверяем, есть ли игрок в списке
			if (!this.gamers[gamer.name]) {
				spawn(Character, {
					...gamer.coords,
					gamerName: gamer.name,
					healthMax: gamer.healthMax,
					health: gamer.health,
					fraction: 'player',
					isPushable: false,
				})
				continue;
			}
			if (gamer.dead) {
				this.gamers[gamer.name].death()
				continue;
			}

			this.gamers[gamer.name].size = 15
			this.gamers[gamer.name].updateName()

			// Пока просто получаем координаты
			this.gamers[gamer.name].moveTo(gamer.coords, ACCURACY)
			this.gamers[gamer.name].direction = gamer.direction
			this.gamers[gamer.name].healthTo(gamer.health)

			if (this.gamers[gamer.name].activeItemName !== gamer.activeItemName) {
				this.gamers[gamer.name].setActiveItem(gamer.activeItemName)
			}
			if (gamer.isAttacked) {
				this.gamers[gamer.name].useActiveItem(gamer.coords)
			}
		}
	}
	onEnemiesUpdate(enemies) {}

	start() {
		this.player.size = 15
		this.player.updateName()
		this.hud.room = this.code
		this.send('start', this.player)

		// Стартуем отрисовку
		this.viewPort.start()
	}

	assignViewPort(viewPort) {
		this.viewPort = viewPort
		// Создаем взаимопривязку
		viewPort.assignGame(this)
	}

	assign(spawnedObject) {
		// Если игрок, запоминаем
		if (spawnedObject instanceof PlayCharacter) {
			this.player = spawnedObject
			this.gamers[this.gamerName] = spawnedObject
			return
		}
	}

	//
	assignHUD(hud) {
		this.hud = hud
	}

	// Вызывается из viewPort'а
	tic() {

		// Обновлем данные игрока
		this.updatePlayer()
	}

	// Для игрока пропускаем 10 кадров
	delayPlayer = DELAY
	updatePlayer() {
		if (this.delayPlayer) {
			this.delayPlayer--
			return
		}
		this.delayPlayer = DELAY
		this.send('move', this.player)
	}

}
