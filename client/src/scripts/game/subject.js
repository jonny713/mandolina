import  { Game, ACCURACY } from './base'
// import PlayCharacter from '../characters/play-character.js'
// import NPC from '../characters/NPC.js'
import Character from '../characters/character.js'
import Bullet from '../bullets/bullet.js'
import { spawn } from '../draw/spawner.js'


export default class Subject extends Game {
	onEnemiesUpdate(enemies) {
		for (let e in enemies) {
			let enemy = enemies[e]
			// Если не создан - создаем
			if (!this.enemies[e]) {
				spawn(Character, {
					...enemy.coords,
					gamerName: e,
					healthMax: enemy.healthMax,
					health: enemy.health,
					fraction: 'monsters',
					isPushable: false,
				})
				continue;
			}

			// Обновляем координаты
			this.enemies[e].moveTo(enemy.coords, ACCURACY)
			this.enemies[e].direction = enemy.direction
			this.enemies[e].healthTo(enemy.health)

			if (this.enemies[e].activeItemName !== enemy.activeItemName) {
				this.enemies[e].setActiveItem(enemy.activeItemName)
			}
			if (enemy.isAttacked && enemy.focusedPos) {
				this.enemies[e].lookTo(enemy.focusedPos)
				this.enemies[e].useActiveItem(enemy.coords)
			}
		}

		for (let e in this.enemies) {
			if (!enemies[e]) {
				this.enemies[e].death()
				delete this.enemies[e]
			}
		}
	}
	onBulletsUpdate(bullets) {}

	assign(spawnedObject) {
		super.assign(spawnedObject)

		// Если перс, то это прочие игроки или противники
		if (spawnedObject instanceof Character) {
			switch (spawnedObject.fraction.name) {
				case 'player':
					this.gamers[spawnedObject.name] = spawnedObject
					break;
				case 'monsters':
					this.enemies[spawnedObject.name] = spawnedObject
					break;
			}
			return
		}

		// Если пуля игрока, убираем урон
		if (spawnedObject instanceof Bullet) {
			if (spawnedObject.fraction.name === 'player') {
				spawnedObject.attack = 0
			}
			return
		}
	}
}
