import Gun from './gun.js'
import BadBigGun from './bad-big-gun.js'

const guns = {
	'classic': Gun,
	'big': BadBigGun,
}

let gunByName = (name) => guns[name] || guns.classic
export default gunByName
