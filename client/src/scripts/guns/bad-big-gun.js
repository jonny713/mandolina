import BadBigAmmo from './../bullets/bad-big-ammo.js'
import AudioEffect from './../audio/audio-effect'
import Gun from './gun.js'

export default class BadBigGun extends Gun {
  constructor(owner) {
    super(owner)

    this.ammoType = BadBigAmmo
    this.ammo = new BadBigAmmo()

    this.clipSize = 999

    this.cooldownPeriod = 300
    this.reloadPeriod = 1200


    this._gunParameters = {
      bulletSpeed: 13,
      bulletAcceleration: 15
    }

		this.audioEffect = new AudioEffect('milli')

  }

	attack(attackParams) {
		// Добавляем немного смещения, чтобы удар шел как бы из персонажа
		return super.attack({
			...attackParams,
			position: {
				// x: attackParams.position.x - Math.sin(attackParams.direction) * 20,
				x: attackParams.position.x,
				// y: attackParams.position.y - Math.cos(attackParams.direction) * 50,
				y: attackParams.position.y,
			}
		})
	}
}
