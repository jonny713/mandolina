import BasicItem from './../basic-item.js'
import Ammo from './../bullets/ammo.js'
import { spawn } from './../draw/spawner.js'
import AudioEffect from './../audio/audio-effect'

export default class Gun extends BasicItem {
  constructor(owner) {
    super()
    this.stackable = false
    this.weight = 3

    this.ammoType = Ammo
    this.ammo = new Ammo()

    this.clipSize = 99999
    this.clip = 99999

    this.isCooldowned = false
    this.cooldownPeriod = 500

    this.isReloaded = false
    this.reloadPeriod = 800

    // Сохраняем ссылку на владельца
    this.owner = owner

    this._gunParameters = {
      bulletSpeed: 150,
      bulletAcceleration: 3
    }

		this.audioEffect = new AudioEffect('shoot')
  }

  attack({ reloadMethod, position, direction, fraction }) {
    if (this.isCooldowned || this.isReloaded)
      return

    if (this.clip > 0) {
      this.clip--
      spawn(this.ammo.associatedBullet, { ...position, direction, fraction }, this._gunParameters.bulletSpeed, this._gunParameters.bulletAcceleration, this.owner)
			this.audioEffect.launch()
    }

    if (this.clip <= 0) {
      this.isReloaded = true
      setTimeout(() => {
        this.reload(reloadMethod(this.ammoType, this.clipSize))
        this.isReloaded = false
      }, this.reloadPeriod)
      return
    }

    this.isCooldowned = true
    setTimeout(() => this.isCooldowned = false, this.cooldownPeriod)
  }

  reload(countBullets) {
    // console.log('RELOAD', countBullets);
    this.clip = countBullets
  }
}
