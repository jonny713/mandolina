export default class AudioEffect {
	constructor(src) {
		this.src = `audios/${src}.mp3` || 'audios/shoot.mp3'

		let audio = new Audio(this.src);
	}

	launch() {
		let audio = new Audio(this.src);
		audio.volume  = .2
		audio.play();
	}
}
