import MovableBlock from './../base-classes/movable-block.js'
import { Fraction, fractions } from './fraction.js'

export default class FractionBlock extends MovableBlock {
  constructor(initialParameters) {
    super(initialParameters)


    if (initialParameters.fraction instanceof Fraction) {
      this.fraction = initialParameters.fraction
    } else {
      this.fraction = fractions.find(f => f.name === initialParameters.fraction) || fractions[0]
    }

    this.fraction.add(this)
  }

  desctruct() {
    this.fraction.delete(this.id)
    super.desctruct()
  }

	get cells() {
		return [this.cell]
	}
}
