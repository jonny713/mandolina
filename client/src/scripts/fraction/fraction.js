class Fraction {
  constructor(name, color) {
    this.name = name
    this.color = color

    this._members = []
    this._friends = []
    this._enemies = []
  }

  get members() {
    return this._members
  }

  get friends() {
    return this._friends.map(friend => friend.members).flat()
  }

  get enemies() {
    return this._enemies.map(enemy => enemy.members).flat()
  }

  add(member) {
    if (this.check(member)) {
      // console.log(`Добавить представителя во фракцию ${this.name} невозможно. Уже состоит в ней`);
      return
    }

    this._members.push(member)
    // console.log(`Представитель ${memberId} добавлен во фракцию ${this.name}`);
  }

  check(memberId) {
    return !!this._members.find(member => member.id === memberId)
  }

  delete(memberId) {
    if (!this.check(memberId)) {
      // console.log(`Удалить представителя из фракции ${this.name} невозможно. Не найден`);
      return
    }
    this._members.splice(this._members.findIndex(member => member.id === memberId), 1)
  }

  addFriend(fraction) {
    if (this._friends.find(f => f.name === fraction.name))
      return

    this._friends.push(fraction)
  }
  deleteFriend(fraction) {
    let indx = this._friends.findIndex(f => f.name === fraction.name)
    if (indx) {
      this._friends.splice(indx, 1)
    }
  }
  isFriend(memberId) {
    return this.check(memberId) || !!this._friends.find(fraction => fraction.check(memberId))
  }

  addEnemy(fraction) {
    if (this._enemies.find(f => f.name === fraction.name))
      return

    this._enemies.push(fraction)

  }
  deleteEnemy(fraction) {
    let indx = this._enemies.findIndex(f => f.name === fraction.name)
    if (indx) {
      this._enemies.splice(indx, 1)
    }
  }
  isEnemy(memberId) {
    return !this.check(memberId) && !!this._enemies.find(fraction => fraction.check(memberId))
  }
}

let neutralFraction = new Fraction('neutral', 'rgb(45, 45, 45)')
let playerFraction = new Fraction('player', 'rgb(15, 13, 140)')
let tradersFraction = new Fraction('traders', 'rgb(15, 130, 14)')
let monstersFraction = new Fraction('monsters', 'rgb(240, 40, 240)')
let raidersFraction = new Fraction('raiders', 'rgb(180, 3, 3)')
let augumsFraction = new Fraction('augums', 'rgb(255, 123, 3)')


// Игрок дружит с торговцами и воюет с монстрами и рейдерами
playerFraction.addFriend(tradersFraction)
playerFraction.addEnemy(monstersFraction)
playerFraction.addEnemy(raidersFraction)
playerFraction.addEnemy(augumsFraction)

// торговцы пофиг на игрока и рейдеров, они воюют с монстрами
tradersFraction.addEnemy(monstersFraction)
tradersFraction.addEnemy(augumsFraction)

// рейдеры воюют с монстрами и игроком, торговцев щадят
raidersFraction.addEnemy(playerFraction)
raidersFraction.addEnemy(monstersFraction)
raidersFraction.addEnemy(augumsFraction)

// Монстры воюют со всеми
monstersFraction.addEnemy(playerFraction)
monstersFraction.addEnemy(tradersFraction)
monstersFraction.addEnemy(raidersFraction)
monstersFraction.addEnemy(augumsFraction)

// Аугументированные воюют со всеми
augumsFraction.addEnemy(playerFraction)
augumsFraction.addEnemy(tradersFraction)
augumsFraction.addEnemy(raidersFraction)
augumsFraction.addEnemy(monstersFraction)


let fractions = [ neutralFraction, playerFraction, tradersFraction, monstersFraction, raidersFraction, augumsFraction ]

export { Fraction, fractions }
