// https://github.com/joeiddon/perlin
let perlin = {
    rand_vect: function(){
        let theta = Math.random() * 2 * Math.PI;
        return {x: Math.cos(theta), y: Math.sin(theta)};
    },
    dot_prod_grid: function(x, y, vx, vy){
        let g_vect;
        let d_vect = {x: x - vx, y: y - vy};
        if (this.gradients[[vx,vy]]){
            g_vect = this.gradients[[vx,vy]];
        } else {
            g_vect = this.rand_vect();
            this.gradients[[vx, vy]] = g_vect;
        }
        return d_vect.x * g_vect.x + d_vect.y * g_vect.y;
    },
    smootherstep: function(x){
        return 6*x**5 - 15*x**4 + 10*x**3;
    },
    interp: function(x, a, b){
        return a + this.smootherstep(x) * (b-a);
    },
    seed: function(){
        this.gradients = {};
        this.memory = {};
    },
    get: function(x, y) {
        if (Object.prototype.hasOwnProperty.call(this.memory, ([x,y])))
            return this.memory[[x,y]];
        let xf = Math.floor(x);
        let yf = Math.floor(y);
        //interpolate
        let tl = this.dot_prod_grid(x, y, xf,   yf);
        let tr = this.dot_prod_grid(x, y, xf+1, yf);
        let bl = this.dot_prod_grid(x, y, xf,   yf+1);
        let br = this.dot_prod_grid(x, y, xf+1, yf+1);
        let xt = this.interp(x-xf, tl, tr);
        let xb = this.interp(x-xf, bl, br);
        let v = this.interp(y-yf, xt, xb);
        this.memory[[x,y]] = v;
        return v;
    }
}


let canvas = document.createElement('canvas');
let ctx = canvas.getContext('2d')

async function generateNoise(GRID_SIZE = 4, RESOLUTION = 64, scale, color) {
  perlin.seed();

  const COLOR_SCALE = 250;

  let pixel_size = canvas.width / RESOLUTION;
  let num_pixels = GRID_SIZE / RESOLUTION;
  let koef = canvas.height / canvas.width

  let data = {}

  let generateLine = (y) => {
    for (let x = 0; x < GRID_SIZE / scale; x += num_pixels / GRID_SIZE){
      let v = parseInt(perlin.get(x, y) * COLOR_SCALE);

      data[v] = v

      ctx.fillStyle = color(v);
      ctx.fillRect(
        x / GRID_SIZE * canvas.width * scale,
        y / GRID_SIZE * canvas.width * scale,
        pixel_size * scale,
        pixel_size * scale
      );

    }
  }
  for (let y = 0; y < GRID_SIZE / scale * koef; y += num_pixels / GRID_SIZE) {
    await new Promise(resolve => setTimeout(resolve, 0))
    generateLine(y)
  }
}

async function generateNoiseImage2(image, GRID_SIZE = 4, RESOLUTION = 20, scale, color, rotation = 0) {
  perlin.seed();

  const COLOR_SCALE = 250;

  let pixel_size = canvas.width / RESOLUTION;
  let num_pixels = GRID_SIZE / RESOLUTION;
  let koef = canvas.height / canvas.width

  let c = { max: 0, min: 0 }

  let generateLine = (y) => {
    for (let x = 0; x < GRID_SIZE / scale; x += num_pixels / GRID_SIZE){
      let v = parseInt(perlin.get(x, y) * COLOR_SCALE);

      c.max = c.max < v ? v : c.max
      c.min = c.min > v ? v : c.min

      ctx.globalAlpha = color(v)

      ctx.drawImage(
        image,
        // координаты смещения
        x / GRID_SIZE * canvas.width % (image.naturalWidth-image.naturalWidth/50), y / GRID_SIZE * canvas.width % (image.naturalHeight-image.naturalWidth/50),
        // ширина обрезки
        pixel_size, pixel_size,
        // координаты
        ...[x / GRID_SIZE * canvas.width * scale, y / GRID_SIZE * canvas.width * scale],
        // размер
        ...[pixel_size * scale, pixel_size * scale,]
      )
    }
  }
  for (let y = 0; y < GRID_SIZE / scale * koef; y += num_pixels / GRID_SIZE) {
    await new Promise(resolve => setTimeout(resolve, 0))
    generateLine(y)
  }
}

async function generateNoiseImage(image, GRID_SIZE = 4, RESOLUTION = 20, scale, color, rotation = 0) {
  perlin.seed();

  const COLOR_SCALE = 250;

  let pixel_size = canvas.width / RESOLUTION;
  let num_pixels = GRID_SIZE / RESOLUTION;
  let koef = canvas.height / canvas.width

  let c = { max: 0, min: 0 }

  let generateLine = (y) => {
    for (let x = 0; x < GRID_SIZE / scale; x += num_pixels / GRID_SIZE){
      let v = parseInt(perlin.get(x, y) * COLOR_SCALE);

      c.max = c.max < v ? v : c.max
      c.min = c.min > v ? v : c.min

      let [width, height] = color(v)
      // console.log(width, height)
      ctx.drawImage(
        image,
        // координаты смещения
        // x / GRID_SIZE * canvas.width % (image.naturalWidth-image.naturalWidth/50), y / GRID_SIZE * canvas.width % (image.naturalHeight-image.naturalWidth/50),
        0,0,
        // ширина обрезки
        image.naturalWidth, image.naturalHeight,
        // pixel_size, pixel_size,
        // координаты
        ...[x / GRID_SIZE * canvas.width * scale, y / GRID_SIZE * canvas.width * scale],
        // размер
        ...[width* scale, height* scale]
      )
    }
  }
  for (let y = 0; y < GRID_SIZE / scale * koef; y += num_pixels / GRID_SIZE) {
    await new Promise(resolve => setTimeout(resolve, 0))
    generateLine(y)
  }

  // console.log(`max: ${c.max}, min: ${c.min}, range: ${c.max - c.min}`)
}

export async function perlinGen(sources, width, height, cb) {
  let size = 4096
  let scale = .04

  canvas.width = Math.min(width, size);
  canvas.height = Math.min(height, size);
  ctx.fillStyle = 'hsla(121, 6%, 74%, 1)';
  ctx.fillRect(0,0,canvas.width,canvas.width);

  let _image = new Image()
  _image.src = canvas.toDataURL()
  cb(_image)

  let alpha = .3
  let cutoff = 10
  let min = 50, sum = 200
  let rand = (x) => (Math.random() + 1) * x - 1
  let fromTo = (v, from, to) => (to - from) * (v + min) / sum + from
  await generateNoise(3, 17, scale * rand(2), (v) => `hsla(${fromTo(v, 105, 121)}deg, ${fromTo(v, 3, 7)}%, ${fromTo(v, 30, 55)}%, ${fromTo(v, -.9, .5)}`)
  _image.src = canvas.toDataURL()
  cb(_image)
  await generateNoise(3, 13, scale * rand(2), (v) => `hsla(121, 6%, 74%, ${fromTo(v, .2, .9)}`)
  _image.src = canvas.toDataURL()
  cb(_image)

  for (let source of sources) {
    let image = new Image()
    image.src = source;

    await new Promise(resolve => image.onload = () => resolve())

    let limit = 1e2
    let c = rand(limit)
    await generateNoiseImage(image, 1.5, 10, scale * rand(4), (v) => {
      c += Math.abs(v);
      if (c < limit / scale) {
        return [0,0]
      }

      let width = rand(3) * c / limit
      c = 0;

      return [width,width]
    })

    _image.src = canvas.toDataURL()
    cb(_image)
  }

    // await generateNoise(6, 20, scale*Math.sqrt(7), (v) => `hsl(${(45 - 31.5) * (v + min) / sum + 31.5}deg, ${(32 - 26) * (v + min) / sum + 26}%, ${(59 - 36) * (v + min) / sum + 36}%`)
    // let _image = new Image()
    // _image.src = canvas.toDataURL()
    // cb(_image)
    //
    // await generateNoiseImage(image, 6, 20, scale*Math.sqrt(9), (v) => v > -80 ? .2 * (v + 160) / sum : 0)
    // _image = new Image()
    // _image.src = canvas.toDataURL()
    // cb(_image)
    //
    // await generateNoiseImage(image, 6, 20, scale*Math.sqrt(11), (v) => v > -20 ? .2 * (v + 160) / sum : 0)
    // _image = new Image()
    // _image.src = canvas.toDataURL()
    // cb(_image)
    //
    // await generateNoiseImage(image, 6, 20, scale*Math.sqrt(7), (v) => v > 30 ? .2 * (v + 160) / sum : 0)
    // _image = new Image()
    // _image.src = canvas.toDataURL()
    // cb(_image)

}
