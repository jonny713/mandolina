
export class BaseFrame {
	constructor(target) {
		this.target = target
	}

	get position() {
		return {
			x: this.target.position.x - this.target.position.x * this.offset.zoom + this.offset.x,
			y: this.target.position.y - this.target.position.y * this.offset.zoom + this.offset.y,
		}
	}

	get size() {
		return (this.target.size - this.target.size * this.offset.zoom)
	}

	get direction() {
		return this.target.direction
	}

	offset = {
		x: 0,
		y: 0,
		zoom: 1,
	}
	setOffset({ x, y, zoom }, { width, height }) {
		this.offset = { x, y, zoom }
	}

	/**
	 * @summary Рисует в контексте себя
	 */
	draw(context) {}
}
