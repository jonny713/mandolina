import { BaseFrame } from './base'

/**
 * @summary фрейм для текста
 */
export class TextFrame extends BaseFrame {
	text = ''

	/**
	 * @summary Рисует в контексте себя
	 */
	draw(context) {
		let { position, direction, size } = this

		context.font = `${size}px serif`
		context.textAlign = 'left'
		// TODO: нужен цвет текста
		// TODO: нужна обводка какая-нибудь

		// if (this.text === 'Room #undefined') {
		// 	context.fillText(`x: ${pos.x}, ${offsetX}, ${pos.x + offsetX}`, 2000, 400);
		// 	context.fillText(`y: ${pos.y}, ${offsetY}, ${pos.y + offsetY}`, 2000, 450);
		// 	context.fillText(this.text, pos.x + offsetX, pos.y + offsetY);
		// 	return
		// }
		context.fillText(this.text, position.x, position.y);
	}
}
