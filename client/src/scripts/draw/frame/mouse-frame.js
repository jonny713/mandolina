import { BaseFrame } from './base'

/**
 * @summary фрейм с индивидуальным расчетом координат под объект мыши
 */
export class MouseFrame extends BaseFrame {
	get position() {
		return {
			x: this.target.position.x,
			y: this.target.position.y,
		}
	}
}
