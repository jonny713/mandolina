import { ImageFrame } from './image-frame'
import { ANIMATION_TYPES, defaultTileMap } from './../tiles/animation-tile-map'

/**
 * @summary фрейм анимаций. Предлагает набор стандартных анимаций и метод расширения
 */
export class AnimationFrame extends ImageFrame {
	constructor(target, animationTileMap = defaultTileMap) {
		super(target)

		this.tileMap = animationTileMap
	}

	get direction() {
		return Math.PI / 2
	}

	handlers = [
		{
			name: 'idle',
			priority: 0,
			cb: ({ sumSpeed }) => sumSpeed < .1,
		},{
			name: 'walk-top',
			priority: 1,
			cb: ({ moveDirection }) => moveDirection > Math.PI/4 && moveDirection < Math.PI/4 + Math.PI/2,
		},{
			name: 'walk-right',
			priority: 1,
			cb: ({ moveDirection }) => moveDirection >= -Math.PI/4 && moveDirection <= Math.PI/4,
		},{
			name: 'walk-bottom',
			priority: 1,
			cb: ({ moveDirection }) => moveDirection > -Math.PI/4 - Math.PI/2 && moveDirection < -Math.PI/4,
		},{
			name: 'walk-left',
			priority: 1,
			cb: ({ moveDirection }) => moveDirection >= Math.PI/4 + Math.PI/2 || moveDirection <= - Math.PI/4 - Math.PI/2,
		},{
			// дефолтный обработчик
			name: 'default',
			priority: Infinity,
			cb: () => true,
		}
	]
	addHandler(name, priority, cb) {
		this.handlers.push({
			name,
			priority,
			cb,
		})

		this.handlers.sort((a, b) => a.priority - b.priority)
	}
	chooseAnimation() {
		let { speed } = this.target

		if (!speed) {
			return this.tileMap.getAnimation(this.handlers[0].name)
		}

		let moveDirection = -Math.atan2(speed.y, speed.x)
		let sumSpeed = Math.abs(speed.y) + Math.abs(speed.x)

		for (let handler of this.handlers) {
			if (handler.cb({ moveDirection, sumSpeed })) {
				return this.tileMap.getAnimation(handler.name)
			}
		}
	}

	index = 0
	// TODO: можно завязать на текущем значении FPS
	// TODO: А еще это должно зависеть от скорости персонажа
	delay = 15
	chooseTile() {
		let animation = this.chooseAnimation()
		if (this.currentAnimation?.name === animation.name) {
			// Подсчитать кадр
			this.index = this.index < animation.len * this.delay - 1 ? this.index + 1 : 0

			// Выбрать тайл
			this.tile = this.tileMap.getTileByType(animation.name, Math.floor(this.index / this.delay))
			// console.log(Math.floor(this.index / this.delay));
			return
		}

		// При смене анимации нужно сбросить счетчик
		this.index = 0
		// Записать новую текущую анимацию
		this.currentAnimation = animation
		// Выбрать первый тайл новой анимации
		this.tile = this.tileMap.getTileByType(animation.name, this.index)
	}

	draw(context) {
		// Вычисляем тайл, который нужно рисовать
		this.chooseTile()

		// Рисуем его с помощью метода в ImageFrame
		super.draw(context)
	}
}
