import { BaseFrame } from './base'

/**
 * @summary фрейм простых гемоетрических форм. Позволяет описать простой набор базовых форм,
 * которые будут рисоваться. В основном нужен для UI
 */
export class GeometryFrame extends BaseFrame {
	_elements = []
	/**
	 * @summary Очищает список форм
	 */
	reset(){
		this._elements = []
	}

	/**
	 * @summary Прекращает отрисовку
	 */
	hide(){}

	/**
	 * @summary Вновь начинает отрисовку
	 */
	show(){}

	addLine(point1, point2, color) {
		this._elements.push((context, { position, direction, size }) => {
			context.beginPath()
			context.moveTo(
				position.x + Math.cos(direction) * size * point1.x,
				position.y - Math.sin(direction) * size * point1.y,
			)
			context.lineTo(
				position.x + Math.cos(direction) * size * point2.x,
				position.y - Math.sin(direction) * size * point2.y,
			)
			context.stroke()
		})
	}

	addRect(point1, point2, color) {
		this._elements.push((context, { position, direction, size }) => {
			context.beginPath()
			context.rect(
				position.x + point1.x* size,
				position.y + point1.y* size,
				Math.abs(point1.x - point2.x) * size,
				Math.abs(point1.y - point2.y) * size,
			)
			// console.log(
			// 	position.x + point1.x,
			// 	position.y + point1.y,
			// 	Math.abs(point1.x - point2.x) * size,
			// 	Math.abs(point1.y - point2.y) * size,
			// );
			if (color) {
				context.fillStyle = color
				context.fill()
				context.fillStyle = 'black'
				return
			}
			context.stroke()
		})
	}

	/**
	 * @summary Рисует в контексте себя
	 */
	draw(context) {
		let { position, direction, size } = this

		for (let element of this._elements) {
			element(context, { position, direction, size })
		}
	}
}
