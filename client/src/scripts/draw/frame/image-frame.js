import { BaseFrame } from './base'
import { ImageTile } from './../tiles/image'

/**
 * @summary фрейм
 */
export class ImageFrame extends BaseFrame {
	constructor(target, tile = new ImageTile(476)) {
		super(target)

		this.tile = tile
	}
	draw(context) {
		let { imageSrc, offsets, croppingSize, coordinates, sizes, offsetsToCenter = [0,0], rotate } = this.tile

		let { position, direction, size } = this
		let x = position.x
		let y = position.y

		if (direction !== Math.PI / 2) {
			context.translate(x, y);
			context.rotate(-direction + (rotate ?? 0));
			context.translate(-x, -y);
			// this.target.direction += .01
		}
		context.drawImage(
			imageSrc,
			// координаты смещения
			...offsets,
			// ширина обрезки
			...croppingSize,
			// координаты
			x-size, y-size,
			// размер
			size * 2 + sizes[0] + 2, size * 2 + sizes[1] + 2
		)

		// Для тестов рисуем центр и направление
		context.ellipse(x, y, 10, 10, 0, 0, 0)

		if (direction !== Math.PI / 2) {
			context.resetTransform();
		}
	}
}
