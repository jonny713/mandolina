import BaseViewPort from './base'
import blockTypes from './../../constructs/blockTypes'
import { perlinGen } from './../perlin-gen.js'

/**
 * @abstract
 * @summary абстрактный класс в цепочке наследования, описывает спавн
 */
export default class SpawnViewPort extends BaseViewPort {
  get background() {
    if (this._background && this._background.ready) {
      return this._background.image
    }

    if (this._background) {
      return null
    }

    this._background = {}
    perlinGen(
      ['textures/grass2.png', 'textures/rock2.png', ],
      // 'textures/grass3.jpg',
      this.arena.x2 - this.arena.x1,
      this.arena.y2 - this.arena.y1,
      (image) => {
        this._background.image = image;
        this._background.ready = true
      }
    )

    return null
  }

	_arena = {}
	get arena() {
		return {
			x1: this._arena.x1,
			y1: this._arena.y1,
			x2: this._arena.x2,
			y2: this._arena.y2,
		}
	}
	updateArena({ x, y }) {
		this._arena = {
			x1: Math.min(this._arena.x1 || 0, x),
			y1: Math.min(this._arena.y1 || 0, y),
			x2: Math.max(this._arena.x2 || 0, x),
			y2: Math.max(this._arena.y2 || 0, y),
		}
	}

	unicIds = []
	_createUnicId() {
		let len = this.unicIds.length
		let lastId = len ? parseInt(this.unicIds[len-1], 16) : 1
		let newId = lastId.toString(16).padStart(8, '0')
		this.unicIds.push(newId)
		return newId
	}

	/**
	 * @summary Основной метод добавления новых игровых объектов во вью-порт
	 */
	spawn(drawConstructor, ...args) {
		// Если нет конструктора или нет метода tic
		if (!drawConstructor || !('tic' in drawConstructor.prototype)) {
			return
		}

		// Защита от пустого массива аргументов
		args = args.length ? args : [{}]

		// Добавляем уникальный id
		args[0].id = this._createUnicId()
		// console.log(...args);

		// Создаем экземпляр
		let spawnedObject = new drawConstructor(...args)

		// Определяем ячейку для объекта
		spawnedObject.cell = this.level.getCell(spawnedObject.position)

		// Определяем слой, на котором будет рисоваться объект
		let layer = this.layers[spawnedObject.drawingLayer] ? spawnedObject.drawingLayer : 1
		this.layers[layer].push(spawnedObject)

		// Обновляем размер арены
		if (spawnedObject.position) {
			this.updateArena(spawnedObject.position);
			// console.log('Обновляем размер арены', JSON.stringify(assignedViewPort.arena));
		}

		return spawnedObject
	}

	// Привязываем уровень к вью-порту
	assignLevel(level) {
		this.level = level

		// Рисуем блоки уровня
		for (let block of level.blockedCells) {
			this.spawn(blockTypes[block.type], ...block.args)
		}
	}
	// Привязываем уровень к вью-порту
	assignCamera(Camera, player) {
		return this.camera = this.spawn(Camera, {}, player)
	}
	// Привязываем уровень к вью-порту
	assignMouse(Mouse) {
		return this.mouse = this.spawn(Mouse)
	}
}
