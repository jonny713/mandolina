import SpawnViewPort from './with-spawn'
import Performance from './../performance.js'

let perf = Performance.counter()
export default class TicViewPort extends SpawnViewPort {

	/**
	* @summary Обновляет глобальное смещение
	*/
	updateOffset() {
		let zoom = (this.mouse.wheel - 25) / 250
		this.offset = {
			x: this.width / 2 - (this.camera.position.x - this.camera.position.x * zoom),
			y: this.height / 2 - (this.camera.position.y - this.camera.position.y * zoom),
			zoom,
		}

		// TODO: Выглядит как дублирование кода
		// Передаем смещение в обработчик мыши
		this.mouse.setOffset({
			x: this.offset.x - this.camera.position.x * zoom,
			y: this.offset.y - this.camera.position.y * zoom,
		})
		perf.breakPoint('offset')
	}
	toLocalCoords({x, y}) {
		return {
			x: x + this.offset.x - this.offset.zoom * (x - this.camera.position.x),
			y: y + this.offset.y - this.offset.zoom * (y - this.camera.position.y),
		}
	}

	fillBackground() {
		const isBackOff = true
		if (isBackOff || !this.background) {
			this.context.fillStyle = "#b9c0b9";
			this.context.fillRect(0, 0, this.width, this.height);
		} else {
			this.context.fillStyle = "black";
			this.context.fillRect(0, 0, this.width, this.height);

			let arena = this.arena;

			let coords1 = this.toLocalCoords({
				x: arena.x1,
				y: arena.y1,
			})
			let coords2 = this.toLocalCoords({
				x: arena.x2,
				y: arena.y2,
			})

			this.context.drawImage(
				this.background,
				// координаты смещения
				0, 0,
				// ширина обрезки
				this.background.naturalWidth, this.background.naturalHeight,
				// координаты
				coords1.x, coords1.y,
				// размер
				coords2.x - coords1.x, coords2.y - coords1.y,
			)
		}
		perf.breakPoint('fillBackground')
	}


	// метод удаления разрушенных объектов
	deleteDestroyed(layer, destroyed) {
		for (let d = 0; d < destroyed.length; d++) {
			layer.splice(destroyed[d] - d, 1)
		}
	}

	tic() {
		perf.breakPoint('start')

		// Если отсутствует камера, ничего не рисуем
		if (!this.camera || !this.mouse) {
			return setTimeout(() => this.tic(), perf.calcFrameTime())
		}

		// Получаем смещение от камеры
		this.updateOffset(this.camera.position, this.mouse.wheel)



		// Очищаем рабочую область
		this.fillBackground()

		// Перебираем слои
		for (let layer of this.layers) {
			this._calculateLayer(layer)
		}
		perf.breakPoint('draw layers')

		// рисуем границы ячеек
		this._drawCells()

		// Откладываем запуск следующего кадра
		setTimeout(() => this.tic(), perf.calcFrameTime())
	}

	/**
	 * @summary Рисует объекты с конкретного слоя
	 * @private
	 */
	_calculateLayer(layer) {
		let deleted = []

		// рисуем объекты слоя
		for (let i = 0; i < layer.length; i++) {
			let drawObject = layer[i]

			// Если объект помечен как удаленный, запоминаем его индекс, чтобы не рисовать и удалить из слоя
			if (drawObject.deleted) {
				deleted.push(i)
				continue
			}

			// TODO: зумом надо управлять во вьюпорте
			drawObject.zoom(this.offset.zoom)

			// выполняем метод TIC конкретного объекта, чтобы тот пересчитал свои параметры
			let frame = drawObject.tic()
			perf.breakPoint('tic object ' + drawObject.constructor.name )

			// Проверяем, виден ли объект из текущей позиции камеры
			if (!this.camera.checkVisible(drawObject)) {
				continue
			}

			if (drawObject === this.hud) {
				this.hud.upd(this.camera.position)
				continue
			}

			frame.setOffset(this.offset, this)
			frame.draw(this.context)
			perf.breakPoint('draw')
		}

		perf.breakPoint(`layer drawed`)

		// Удаляем из слоя объекты
		this.deleteDestroyed(layer, deleted)
		perf.breakPoint('deleteDestroyed')
	}

	/**
	 * @summary Рисует границы ячеек
	 * @private
	 */
	_drawCells(isCellsDrawing = false) {
		/*console.log('-  ' + this.cells[0].map((cell, i) => i+ (i>9?'':' ')).join(' '));*/
		if (!isCellsDrawing || !this.level) {
			return
		}

		let second = (new Date().getSeconds()) % 2
		for (let y in this.level.cells) {
			for (let x in this.level.cells[y]) {
				let cell = this.level.cells[y][x]
				// console.log(cell);
				// throw Error('Пыщь')

				let personalOffsetX = this.offset.x - this.offset.zoom *(x * this.level.cellSize + this.level.offset.x - this.camera.position.x) + 2
				let personalOffsetY = this.offset.y - this.offset.zoom *(y * this.level.cellSize + this.level.offset.y - this.camera.position.y) + 2

				// this.context.font = '12px Arial'
				// this.context.fillStyle = 'black'
				// this.context.fillText(
				//   `${second ? x : y}`,
				//   x * this.level.cellSize + this.level.offset.x + personalOffsetX,
				//   y * this.level.cellSize + this.level.offset.y + personalOffsetY,
				//   10
				// )
				this.context.strokeStyle = "light-grey";

				this.context.strokeRect(
					x * this.level.cellSize + this.level.offset.x + personalOffsetX,
					y * this.level.cellSize + this.level.offset.y + personalOffsetY,
					this.level.cellSize + 8,
					this.level.cellSize + 8,
				)

				if (!cell.type.id) {
					continue
				}

				this.context.fillStyle = cell.type.id === 2 ? "rgba(255,36,0,.65)" : "rgba(253,233,16,.65)";
				this.context.fillRect(
					x * this.level.cellSize + this.level.offset.x + personalOffsetX + 2,
					y * this.level.cellSize + this.level.offset.y + personalOffsetY + 2,
					this.level.cellSize - 2,
					this.level.cellSize - 2,
				)
			}
		}

		perf.breakPoint('draw cells')
	}
}
