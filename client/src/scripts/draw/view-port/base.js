
/**
 * @abstract
 * @summary Базовый абстрактный класс, описывает работу с канвасом и слоями
 */
export default class BaseViewPort {
	constructor(canvas) {
		this.canvas = canvas
		this.context = this.canvas.getContext('2d');

		// Отслеживаем изменения размеров канваса
		this._watchingCanvas()

		// TODO: Можно попробовать переписать на объекты с ключами в виде id
		// Слои - для управления порядком отрисовки
		this.layers = [ [], [], [], [], [] ]
	}
	_watchingCanvas() {
		// Снимаем прочие слушатели событий
		window.removeEventListener('resize', () => {})

		// Создаем новое
		window.addEventListener('resize', () => this._resize())

		// Сразу вызываем resize, без него масштаб неправильный
		this._resize()
	}
	_resize() {
		this.width = document.documentElement.clientWidth
		this.height = document.documentElement.clientHeight

		if (this.hud) {
			this.hud.resize({
				width: this.width,
				height: this.height,
			})
		}
	}

	set width(width) {
		this.canvas.width = width
	}
	get width() {
		return this.canvas.width
	}
	set height(height) {
		this.canvas.height = height
	}
	get height() {
		return this.canvas.height
	}

	/**
	 * @todo экспериментальная штука
	 */
	assignWebGL(webGL) {
		this.webGL = webGL
	}
	// Привязка пользовательского интерфейса
	assignHUD(hud) {
		this.hud = hud
		this.hud.resize({
			width: this.width,
			height: this.height,
		})
	}
}
