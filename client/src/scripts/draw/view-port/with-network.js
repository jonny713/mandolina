import TicViewPort from './with-tic'
import FractionBlock from './../../fraction/fraction-block.js'

export default class NetworkViewPort extends TicViewPort {
	spawn(...args) {
		let spawnedObject = super.spawn(...args)

		// Привязываем активный объект к игре
		if (spawnedObject instanceof FractionBlock) {
			this.game.assign(spawnedObject)
		}

		return spawnedObject
	}

	// Привязываем уровень к вью-порту
	assignGame(game) {
		this.game = game
	}

	tic() {
		super.tic()

		// Обновляем сетевую игру
		this.game.tic()
	}

	/**
	 * @summary Действия, необходимые при старте игры
	 */
	start() {
		this.tic()
	}
}
