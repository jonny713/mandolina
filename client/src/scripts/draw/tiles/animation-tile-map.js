import { TileMap } from './tile-map'

const HALF_DIRECTIONS = [ 'top-left', 'top-right', 'bottom-right', 'bottom-left' ]
const DIRECTIONS = [
	'left', 'top', 'right', 'bottom',
	// ...HALF_DIRECTIONS,
]
const ANIMATION_TYPES = [
	// ...new Array(DIRECTIONS.length).fill('target-').map((name, i) => name + DIRECTIONS[i]),
	...new Array(DIRECTIONS.length).fill('walk-').map((name, i) => name + DIRECTIONS[i]),
	// ...new Array(DIRECTIONS.length).fill('attack-').map((name, i) => name + DIRECTIONS[i]),
].reduce((a, i) => a = {...a,[i]:i}, {})
console.log(ANIMATION_TYPES);

// const animationsExample = {
// 	'idle': [28, 31, 36, 31],
// 	'walk-top': [...new Array(10).fill(71).map((t,i) =>t+i), ],
// 	'walk-right': [...new Array(10).fill(43).map((t,i) =>t+i), ],
// 	'walk-bottom': [...new Array(10).fill(127).map((t,i) =>t+i), ],
// 	'walk-left': [...new Array(10).fill(99).map((t,i) =>t+i), ],
// }
const animationsExample = {
	'idle': [...new Array(3).fill(72).map((t,i) =>t+i), ],
	'walk-top': [...new Array(4).fill(24).map((t,i) =>t+i), ],
	'walk-right': [...new Array(4).fill(12).map((t,i) =>t+i), ],
	'walk-bottom': [...new Array(4).fill(0).map((t,i) =>t+i), ],
	'walk-left': [...new Array(4).fill(36).map((t,i) =>t+i), ],
}

class AnimationTileMap extends TileMap {
	constructor(initialParameters) {
		super(initialParameters)

		let { animations } = initialParameters

		this.animations = animations || animationsExample
	}

	getTileByType(animationName, index) {
		let animationSet = this.animations[animationName] || []

		let tileNumber = animationSet[index] || 0

		return this.getTile(tileNumber)
	}

	getAnimation(animationName) {
		let animationSet = this.animations[animationName] || []

		return {
			name: animationName,
			len: animationSet.length
		}
	}
}

// const defaultTileMap = new AnimationTileMap({
// 	src: 'textures/char_3.png',
// 	width: 1792,
// 	height: 1280,
// 	tileWidth: 1792 / 14,
// 	tileHeight: 1280 / 10,
// 	xOffset: -10,
// 	yOffset: -3,
// 	offsetsToCenter: [-2,-2]
// })
const defaultTileMap = new AnimationTileMap({
	src: 'textures/dogs_1.png',
	width: 1152,
	height: 864,
	tileWidth: 1152 / 12,
	tileHeight: 864 / 9,
	xOffset: -10,
	yOffset: -3,
	offsetsToCenter: [-2,-2]
})

export {
	AnimationTileMap,
	defaultTileMap,
	ANIMATION_TYPES,
}
