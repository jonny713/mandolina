/**
 *
 *
 */
class TileMap {
  constructor({ src = 'textures/tile_map_2.png', width = 960, height = 960, tileWidth = 32, tileHeight = 32, xOffset = 0, yOffset = 0, offsetsToCenter = [0,0] } = {}) {
    this.image = new Image()
    this.image.src = src
    this.image.onload = () => this.ready = true

    this.tileWidth = tileWidth
    this.tileHeight = tileHeight
    this.rowCount = Math.round(height / tileHeight)
    this.columnCount = Math.round(width / tileWidth)
    this.ready = false

    this.xOffset = xOffset
    this.yOffset = yOffset

    this.offsetsToCenter = offsetsToCenter
  }

  getTile(num) {
    let x = num % this.columnCount
    let y = Math.floor(num / this.columnCount)
    return {
      // объект с изображением
      imageSrc: this.image,
      // координаты смещения
      offsets: [ this.tileWidth * x, this.tileHeight * y ],
      // ширина обрезки
      croppingSize: [ this.tileWidth, this.tileHeight ],
      // координаты
      coordinates: [ this.xOffset, this.yOffset ],
      // размер
      sizes: [ 0, 0 ],
      // размер
      offsetsToCenter: this.offsetsToCenter
    }
  }
}

const defaultTileMap = new TileMap()

export {
  TileMap,
  defaultTileMap
}
