import { defaultTileMap } from './tile-map.js'

export class ImageTile {
	constructor(tileNumber = 0, tileMap = defaultTileMap) {
		// Получаем из тайловой карты параметры и ссылку на изображение
		let { imageSrc, offsets, croppingSize, coordinates, sizes,offsetsToCenter } = tileMap.getTile(tileNumber)

		this.imageSrc = imageSrc
		this.offsets = offsets
		this.croppingSize = croppingSize
		this.coordinates = coordinates
		this.sizes = sizes
		this.offsetsToCenter = offsetsToCenter
	}
}
