import vueConnector from '../game/vue-connector'

let defaultCounter

export default class Performance {
  static counter(fps) {
    return defaultCounter || (defaultCounter = new Performance(fps))
  }

  constructor(fps = 60) {
    this.fps = fps

    this.frameTimes = []
    this.breakpoints = []
    this.lastTime = 0

    this.logLevel = 0
  }

  get fps() {
    return this._fps
  }
  set fps(value) {
    this._fps = value
    this.period = 1e3 / value
    return this._fps
  }

  get fpsElement() {
    if (!this._fpsElement) {
      this._fpsElement = vueConnector.fpsMeter
    }
    return this._fpsElement
  }

  breakPoint(name) {
    if (name == 'start') {
      this.breakpoints = []
      this.lastTime = performance.now()
      this.frameStart = this.lastTime
    }
    if (name == 'end') {
      return this.breakpoints
    }
    let nowTime = performance.now()
    let deltaTime = nowTime - this.lastTime
    if (deltaTime > .1) {
      this.breakpoints.push({
        name,
        time: deltaTime
      })
    }
    this.lastTime = nowTime
  }

  calcFrameTime() {
    let frameStart = this.frameStart
    let frameEnd = performance.now()
    let frameTime = frameEnd - frameStart
    this.frameTimes.push(frameEnd - frameStart)
    if (this.frameTimes.length > 60) {
      this.frameTimes.shift()
    }

    let averageTime = this.frameTimes.reduce((a, ft) => a += ft, 0) / this.frameTimes.length
    let fps = 1000 / averageTime
    // console.log(`время кадра: ${frameTime}`);
    if (frameTime > this.period) {
      switch (this.logLevel) {
        case 3: {
          console.error(`ВРЕМЯ КАДРА БОЛЬШЕ ПЕРИОДА ${this.period} (Среднее последних 60 кадров: ${averageTime})`)
          console.table(this.breakPoint('end'))
          break
        }
        case 2: {
          console.error(`ВРЕМЯ КАДРА БОЛЬШЕ ПЕРИОДА ${this.period} (Среднее последних 60 кадров: ${averageTime})`)
          break
        }
        case 1: {
          console.log(`Время кадра: ${frameTime}`)
          break
        }
      }

      // console.log(`Инфа по точкам останова`)
      // console.table(this.breakPoint('end'))

    }

    if (fps < this.fps) {
      // this.fpsElement.style.backgroundColor = fps < this.fps / 2 ? '#f5c4b5' : 'rgb(235, 255, 73)'
      // this.fpsText.textContent = fps.toFixed(1)
      this.fpsElement.count = fps
    } else {
      // this.fpsElement.style.backgroundColor = '#c5e384'
      // this.fpsText.textContent = this.fps
      this.fpsElement.count = this.fps
      //
    }

    return this.period > frameTime ? this.period - frameTime : 0
  }
}


// let breakpoints = []
// let lastTime = 0
// const breakPoint = (name) => {
//   if (name == 'start') {
//     breakpoints = []
//     lastTime = performance.now()
//
//   }
//   if (name == 'end') {
//     return breakpoints
//   }
//   let nowTime = performance.now()
//   let deltaTime = nowTime - lastTime
//   if (deltaTime > .1) {
//     breakpoints.push({
//       name,
//       time: deltaTime
//     })
//   }
//   lastTime = nowTime
// }
