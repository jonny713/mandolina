import Mouse from '../inputs/mouse.js'
import Camera from '../camera.js'
import FractionBlock from './../fraction/fraction-block.js'

const unicIds = []
const createUnicId = () => {
  let lastId = unicIds.length ? parseInt(unicIds[unicIds.length-1], 16) : 1
  let nowId = (lastId + 1).toString(16).padStart(8, '0')
  unicIds.push(nowId)
  return nowId
}

let assignedViewPort
let assignedGame
let hackSpawn
/**
 * @summary Глобальный метод добавления рисуемого объекта на ассоциированный вью-порт
 * @param drawConstructor - конструктор класса рисуемого объекта
 * @param args - аргументы, передаваемые в конструктор
 * @returns объект переданного класса
 */
export function spawn(drawConstructor, ...arg) {
	if (hackSpawn) {
		return hackSpawn(drawConstructor, ...arg)
	}
  // spawn(drawConstructor, ...arg) {
  if (!arg.length)
    arg = [{}]

  // TODO: Добавить проверку наличия метода tic() и возвращаемых им параметров
  arg[0].id = createUnicId()
  // arg[0].spawnInViewPort = (...args) => assignedViewPort.spawn(...args)
  // console.log(...arg);

  let spawnedObject = new drawConstructor(...arg)

  spawnedObject.cell = assignedViewPort.level.getCell(spawnedObject.position)

  // Определяем слой, на котором будет рисоваться объект
  let layer = assignedViewPort.layers[spawnedObject.drawingLayer] ? spawnedObject.drawingLayer : 1
  assignedViewPort.layers[layer].push(spawnedObject)


  // Привязываем объект камеры в соответствующее поле
  if (spawnedObject instanceof Camera) {
    assignedViewPort.camera = spawnedObject
  }

  // Привязываем объект мыши в соответствующее поле
  if (spawnedObject instanceof Mouse) {
    assignedViewPort.mouse = spawnedObject
  }

  // Привязываем активный объект к игре
  if (spawnedObject instanceof FractionBlock) {
    assignedGame.assign(spawnedObject)
  }

  // Обновляем размер арены
  // console.log('Новый объект', spawnedObject);
  if (spawnedObject.position) {
    assignedViewPort.arena = spawnedObject.position;
    // console.log('Обновляем размер арены', JSON.stringify(assignedViewPort.arena));
  }

  return spawnedObject
}

export function spawnWith(target, drawConstructor, ...arg) {
	return spawn(drawConstructor, ...arg)
}

export function assignGlobalSpawnMethodToViewPort(viewPort) {
  assignedViewPort = viewPort
}
export function assignGlobalSpawnMethodToGame(game) {
  assignedGame = game
}

// TODO: временный хак
export function updateSpawn(spawn) {
	hackSpawn = spawn
}
