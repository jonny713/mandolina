const TYPES = {
  0: {
    id: 0,
    title: 'Проходимая',
    color: 'white',
  },
  1: {
    id: 1,
    title: 'Сосед непроходимой',
    color: 'yellow',
    passageCheck: function(lastPosition, position, isLight) {
      if (isLight) {
        return {
          position,
          cell: this
        }
      }
      return {
        position: {
          x:
            this.right.type.id === 2 ? Math.min(lastPosition.x, position.x) :
            this.left.type.id === 2 ? Math.max(lastPosition.x, position.x) :
            position.x,
          y:
            this.down.type.id === 2 ? Math.min(lastPosition.y, position.y) :
            this.up.type.id === 2 ? Math.max(lastPosition.y, position.y) :
            position.y
        },
        cell: this
      }
    }
  },
  2: {
    id: 2,
    title: 'Непроходимая',
    color: 'red',
  }
}

export default TYPES
