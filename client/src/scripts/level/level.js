import construct from './construct.js'
// import construct from './../constructs/construct.js'
import Cell from './cell.js'
// const construct = require('./construct.js');

/*
Данный класс позволяет поделить уровень на сетку. Каждая ячейка сетки будет описывать квадратную зону N*N и иметь ряд свойств, например - проходимость
Ячейки - объекты некоторого класса. Каждая ячейка имеет представление о своих соседях. Поэтому переход в соседние ячейки осуществляется по ссылке

Пример: имеется поле 8*8. Персонажи помечены х, стены - с
[.][.][.][.][.][.][.][.]
[.][.][.][.][х][.][.][.]
[.][.][.][.][.][.][.][.]
[.][.][.][.][.][.][.][.]
[.][.][.][.][.][.][.][.]
[.][.][.][с][.][.][.][.]
[.][х][.][с][.][.][.][.]
[.][.][.][с][.][.][х][.]

Рассчитываем, какие персонажи кого видят. Нижне-левый запрашивает ячейку над ним на прозрачность.
Потом идет вправо-вверх-вверх-вправо-вверх-вправо-вверх, находит ячейку с верхним персонажем. Вывод - персонаж видим.
ПОдобная же трассировка до нижне-правого персонажа наткнется на стену, значит персонаж не виден
*/

function analyze(lvl) {
  for (let y in lvl) {
    for (let x in lvl[y]) {
      if (lvl[y][x] == 2) {
        continue
      }

      if (lvl[y][x-1] == 2 || lvl[y][+x+1] == 2) {
        lvl[y][x] = 1
        continue
      }

      if (lvl[y-1] && lvl[y-1][x] == 2) {
        lvl[y][x] = 1
        continue
      }

      if (lvl[+y+1] && lvl[+y+1][x] == 2) {
        lvl[y][x] = 1
        continue
      }
    }
  }


  return lvl
}

class Level {
  constructor({ levelName, width, height, cellSize = 13, offset = { x: 0, y: 0 }, blocks } = {}) {

    let [blockedCells, passableCells] = construct({
      name: levelName,
      offset: offset,
      size: cellSize * 2,
      blocks,
    })
    this.blockedCells = blockedCells

    // Предположим, что уровен создается из некоторых стартовых данных
    // Эти данные мы пропускаем через определенную функцию и на выходе получаем двумерный массив с типами ячеек
    const cells = analyze(passableCells)

    // Размер ячейки в пикселях
    this.cellSize = cellSize

    // Смещение относительно глобальных координат
    this.offset = offset

    this.cells = cells.map((row, y) =>
      row.map((type, x) => new Cell({
          coord: { x, y },
          type,
          level: this
        })
      )
    )

    /*console.log('-  ' + this.cells[0].map((cell, i) => i+ (i>9?'':' ')).join(' '));*/
    for (let y in this.cells) {
      // console.log(y  + (y>99?' ':y>9?'  ':'   ') + this.cells[y].map(cell => cell.type.id).join(' '));
      for (let x in this.cells[y]) {
        let cell = this.cells[y][x]
        let leftCell = this.cells[y][x - 1] || null
        let rightCell = this.cells[y][+x + 1] || null
        let upCell = this.cells[y - 1] ? this.cells[y - 1][x] : null
        let downCell = this.cells[+y + 1] ? this.cells[+y + 1][x] : null

        if (leftCell) {
          cell.addNeighbor(leftCell, 'left')
        }
        if (upCell) {
          cell.addNeighbor(upCell, 'up')
        }
        if (rightCell) {
          cell.addNeighbor(rightCell, 'right')
        }
        if (downCell) {
          cell.addNeighbor(downCell, 'down')
        }
      }
    }
  }

  getCellIndexes(position) {
    let x = Math.floor((position.x - this.offset.x) / this.cellSize)
    let y = Math.floor((position.y - this.offset.y) / this.cellSize)

    if (!this.cells[y] || !this.cells[y][x]) {
      throw Error('outside from level')
      // console.error('outside from level');
    }

    return { x, y }
  }

  getCell(position) {
    const { x, y } = this.getCellIndexes(position)
    return this.cells[y][x]
  }
}




// const level = new Level({ width: 26, height: 26, offset: { x: -150, y: -200 } })
//
// // Получаем ячейку по координатам, привязываем ячейку к объекту тестового персонажа
// const char = {
//   cell: level.getCell({ x: 55, y: 55.6})
// }
// console.dir(char, {depth: 1})
//
// console.dir(char.cell.check({ x: 55, y: 55.6}, { x: -135, y: 55.6 }), {depth: 1})

export default Level
// const {
//   performance
// } = require('perf_hooks');
//
// // Перемещаем персонажа на некоторые небольшие координаты
// function test(ITERATIONS = 1e5) {
//   const level = new Level({ width: 10, height: 10, offset: { x: -150, y: -200 } })
//
//   // Получаем ячейку по координатам, привязываем ячейку к объекту тестового персонажа
//   const char = {
//     cell: level.getCell({ x: 55, y: 55.6})
//   }
//
//   let timeStart = performance.now()
//   for (let i = 0; i < ITERATIONS; i++) {
//     char.cell.check({ x: 55, y: 55.6}, { x: -135, y: 55.6 })
//   }
//   return performance.now() - timeStart
// }
//
// let min = Infinity
// let max = 0
// let sum = 0
// for (let i = 0; i < 1e3; i++) {
//   let time = test(1500)
//   console.log(`min: ${min = Math.min(min, time)}; average: ${(sum += time)/(i + 1)}; max: ${max = Math.max(max, time)};`);
// }
