import pako from 'pako'

const alphabet = {
  "А": '58caf9',
  "Б": '5e8e9e',
  "В": '5e9a9e',
  "fig": 'b.11.1e00013000980040002000000000000000020000001',
  maze: '043SpNOt0J7SkNCw0JjSldOv05XTu9C+07DSidGq07/Uj9Cq0r3QvNGV0JXUhdOg0J/Rq9Gr0bzSjNCs0ZDShNCd0JzRq9Go0JLQo9G40Y/Rk9Gt0YvSktG60o/StNOU06fQl9Kl1IfQoNOy0KTTp9GS0oXQv9Gp0LnRodCa0rTSu9GZ0JrSktOY0JXSidOa1IvQl9Ga0JPSldCw0oHRtNOv0ZLSldCv04fTrNCR0qHQl9O60a/QsNO70p3SuNKl0ZbTmtG60Z/Rg9Gg06PSkNKi0ZTRrtCf0KDTstOZ0oPQotGL0oTRt9Ko0pPStNOT0YjShNGa057UhdGk07fSidCh0YLSrtGq0KPRgtOC0qPQm9O60qbTp9KX1ILUhNOq06XThtCx0aLRl9G+0pfSp9Ga0JDQpNKP063QoNG80bHTq9CR',
  base: '073Tp9GR0JrSkNCw0KDQldOg04vTiNG+07HSrNC304rUj9GF0pLQsNOk0KnQp9SP0Z/RpNCb1I/RvNCS1InRn9G90LLTlNON0ajSutOa0abSvNC20ZrRoNCg0pfTptC10KnRiNO00JjRiNOW0oDTuNGi0YDUhNKx1IfRsdKo0ZjRoNKA0ZHRuNOx0KfSqtCd0a7ToNGl0q3QrdKX0bHUgNOh0prUjNCr0JrTrtCv0JLSktGB0prTktCg0LXSsNGb0bDQoNC+0J7RqtO70KfQmdOr0LPThdOl0J/Rp9SM0JHRttOL06rTn9Cx0LjTiNCU05zRrdGM0p/Qow==',
  arena: '073Tp9GB0KHQkNCw0JzQlNGR0L/SudGj0pDQrtCW1I/RhtGA1IHQqdKq0Y3QkdOr06/SudCt0r3SitCS0azTgdKQ0ZDQsNCg0JjQlNCS0pHRkNCw0KDQmNSM0JbTltKP0YnQmtOu0Jc=',
  "walls": '1f.1f.1fffffffe0000000c00000058000001b000000260000004c000000180000003000000060000000c000000180000003000000060000000c000000180000003000007860000000c000000180000003000000064000000c803b80990041013380820261000404c200000980000073000000060000000ffffffff'
}
const maze = '043SpNOt0J7SkNCw0JjSldOv05XTu9C+07DSidGq07/Uj9Cq0r3QvNGV0JXUhdOg0J/Rq9Gr0bzSjNCs0ZDShNCd0JzRq9Go0JLQo9G40Y/Rk9Gt0YvSktG60o/StNOU06fQl9Kl1IfQoNOy0KTTp9GS0oXQv9Gp0LnRodCa0rTSu9GZ0JrSktOY0JXSidOa1IvQl9Ga0JPSldCw0oHRtNOv0ZLSldCv04fTrNCR0qHQl9O60a/QsNO70p3SuNKl0ZbTmtG60Z/Rg9Gg06PSkNKi0ZTRrtCf0KDTstOZ0oPQotGL0oTRt9Ko0pPStNOT0YjShNGa057UhdGk07fSidCh0YLSrtGq0KPRgtOC0qPQm9O60qbTp9KX1ILUhNOq06XThtCx0aLRl9G+0pfSp9Ga0JDQpNKP063QoNG80bHTq9CR'

/**
 * @summary Получение массива из Base64 строки
 */
function arrayFromBase(base) {
  try {
    let uintArr = (str => {
      let result = []
      for (let i = 0; i < str.length; i++ ) result.push(str[i].charCodeAt() - 1040)
      return result
    })(decodeURIComponent(escape(atob(base))))

    return JSON.parse(pako.inflate(uintArr, { raw:true, to: 'string'}))
  } catch (e) {
    console.error(e)
    return []
  }
}

function construct({ name, offset, size, blocks }) {
  if (!alphabet[name]) {
    console.error(`Not in alphabet ${name}, ${offset.x}, ${offset.y}`)
    return
  }
  let arr = arrayFromBase(alphabet[name])

  let elems = []
  let passableCells = []
  try {
    for (let y = 0; y < arr.length ; y++) {
      let row = []
      for (let x = 0; x < arr[y].length; x++) {
        let cell = arr[y][x] === '0' ? 0 : 2
        row.push(cell, cell/*, cell*/)

        if (arr[y][x] === '0') {
          continue
        }
        elems.push({
          type: arr[y][x],
          args: [{x: size/2 + x*size + offset.x, y: size/2 + y*size + offset.y}]
        })
      }
      passableCells.push([...row], [...row]/*, [...row]*/)
    }
  } catch (e) {
    console.error(e)
  }

  return [elems, passableCells]
}

export default construct
