import TYPES from './cell-types.js'

/**
 * @typedef {object} Point
 * @property {number} x - абсцисса
 * @property {number} y - ордината
 */

/**
 * @summary Вычисление угол наклонения прямой между двумя точками
 * @param {Point} point1 - первая точка
 * @param {Point} point2 - вторая точка
 */
function calcAngle(point1 = { x: 0, y: 0 }, point2 = { x: 0, y: 0 }) {
  // console.log(point1, point2);
  return (point1.x - point2.x)/((point1.y - point2.y) || 1)
}

/**
 * @summary Получение направления в зависимости от первоначального угла
 * @param {Cell} source - источник
 * @param {Point} target - цель
 * @param {number} angle - начальный угол
 */
function getDirection(source = { x: 0, y: 0 }, target = { x: 0, y: 0 }, angle = 0) {
  let dir = 'stay'
  let delta, _delta
  switch (Math.sign(source.x - target.x)) {
    // мы правее цели
    case 1:
      delta = Math.abs(angle - calcAngle(source.left, target))
      dir = 'left'
      break;
    // мы левее цели
    case -1:
      delta = Math.abs(angle - calcAngle(source.right, target))
      dir = 'right'
      break;
  }

  switch (Math.sign(source.y - target.y)) {
    // мы ниже цели
    case 1:
      if (delta === undefined) {
        dir = 'up'
        break;
      }
      _delta = Math.abs(angle - calcAngle(source.up, target))
      if (_delta < delta) {
          dir = 'up'
      }
      break;
    // мы выше цели
    case -1:
      if (delta === undefined) {
        dir = 'down'
        break;
      }
      _delta = Math.abs(angle - calcAngle(source.down, target))
      if (_delta < delta) {
          dir = 'down'
      }
      break;
  }
  return dir
}

/**
 * @typedef {object} Cell
 * @property {number} x - абсцисса
 * @property {number} y - ордината
 * @property {Level} level - ссылка на уровень
 * @property {CellType} type - ссылка на тип ячейки
 */
class Cell {
  constructor({ coord, type, level }) {
    this.level = level

    this.x = coord.x
    this.y = coord.y

    this.type = TYPES[type]

    /**
     * @summary Проверка возможности перемещения внутри ячейки
     * @param {Point} sourcePosition - исходная позиция
     * @param {Point} position - желаемые координаты
     * @param {boolean} isLight - свойство, определяющее возможность свободного прохода через переходные ячейки с типом 1
     * @returns {{ position: Point, cell: Cell }}
     * @property {Point} position - вычисленные доступные координаты
     * @property {Cell} cell - ячейка, в которой расположены указанные координаты
     */
    this.passageCheck = TYPES[type].passageCheck || function(sourcePosition, position, /*isLight*/) {
      return {
        position,
        cell: this
      }
    }
  }

  /**
   * @summary метод для добавления соседей
   * @param {Cell} cell - сосед
   * @param {Enum<'up', 'down', 'left', 'right'>} pos - его относительное расположение
   */
  addNeighbor(cell, pos) {
    if (!['up', 'down', 'left', 'right'].includes(pos)) {
      return
    }
    this[pos] = cell
  }

  /**
   * @summary Сравнение переданных координат с данной ячейки и соседями на совпадение
   * @param {Point} position точка, с координатами которой происходит сравнение
   * @returns {boolean}
   */
  comparePosition(position) {
    try {
      const target = this.level.getCell(position)
      return this.compareCellWithZone(target)
    } catch (e) {
      return false
    }
  }

  /**
   * @summary Сравнение переданной ячейки с данной и соседями на совпадение
   * @param {Cell} cell ячейка, с которой происходит сравнение
   * @returns {boolean}
   */
  compareCellWithZone(cell) {
    return cell === this || cell === this.up || cell === this.right || cell === this.down || cell === this.left
  }

	compareWithObject(near, oldCell) {
		if (!near.cell && !near.cells) {
			return false
		}
		if (!near.cells) {
			return near.cell === this
		}
		// Если объект занимает несколько клеток, проверяем все
		for (let cell of near.cells) {
			if (cell === this) {
				return true
			}
		}
		// Хак для очень быстрых пуль, что могут проскочить ячейку. Если передана прошлая, проверяем, не является ли она соседней
		if (!oldCell || oldCell === this.up || oldCell === this.left || oldCell === this.down || oldCell === this.right) {
			return false
		}

		// Если не является, пытаемся проверить пересечение с соседней в сторону старой
		if (Math.abs(this.x - oldCell.x) > Math.abs(this.y - oldCell.y)) {
			return this[this.x - oldCell.x > 0 ? 'left': 'right'].compareWithObject(near, oldCell)
		} else {
			return this[this.y - oldCell.y > 0 ? 'up': 'down'].compareWithObject(near, oldCell)
		}
	}

  /**
   * @summary Рекурсивное вычисление возможности переместиться в указанный position
   * @param {Point} sourcePosition - исходная позиция
   * @param {Point} position - желаемые координаты
   * @param {boolean} isLight - свойство, определяющее возможность свободного прохода через переходные ячейки с типом 1
   * @param {number} angle - угол наклонения прямой между точками sourcePosition и position
   * @returns {{ position: Point, cell: Cell }}
   * @property {Point} position - вычисленные доступные координаты
   * @property {Cell} cell - ячейка, в которой расположены указанные координаты
   */
  rayCast(sourcePosition, position, isLight = false, angle = calcAngle(sourcePosition, position)) {
    // Клонируем координаты, чтобы избежать изменений по ссылке
    let newPosition = { ...sourcePosition }

    // Пробуем получить ячейку. Если выдает ошибку, возвращаем исходные координаты
    // TODO: Можно не перевычислять целевую ячейку в каждом последующем вызове
    let target
    try {
      target = this.level.getCellIndexes(position)
    } catch (e) {
      return {
        position: newPosition,
        cell: this
      }
    }

    // Если целевые координаты в этой ячейки, то проверяем, можно ли в ней двигаться в указнные стороны
    if (this.x === target.x && this.y === target.y) {
      return this.passageCheck(newPosition, position, isLight)
    }
    // console.log(target);

    // Получаем направление, в котором следует двигаться
    let direction = getDirection(this, target, angle)
    // console.log(`Выбрано направление ${direction} из (${this.x};${this.y}) в (${this[direction].x};${this[direction].y})`);

    // Проверяем возможность переместиться в направлении указанной клетки
    // TODO: стоит перенести вычисление проходимости в типы ячеек
    if (this[direction].type.id === 2) {
      // console.log(`В сторону ${direction} не пройти`, newPosition);
      return {
        position: newPosition,
        cell: this
      }
    }

    // Перемещаемся в высчитанную клетку
    let axis = ['up', 'down'].includes(direction) ? 'y' : 'x'
    newPosition[axis] += (['up', 'left'].includes(direction) ? -1 : 1) * this.level.cellSize
    return this[direction].rayCast(newPosition, position, isLight, angle)
  }

  /**
   * @summary Проверка видимости указанной точки
   * @param {Point} sourcePosition - исходная позиция
   * @param {Point} position - искомая точка
   */
  isOverview(sourcePosition, position) {
    let { position: { x, y } } = this.rayCast(sourcePosition, position, true)

    return position.x == x && position.y == y
  }
}

export default Cell
