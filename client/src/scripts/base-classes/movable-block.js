import StyledBlock from './styled-block.js'

export default class MovableBlock extends StyledBlock {
  constructor(initialParameters){
    super(initialParameters)
    this.crossable = true

    this.drawingLayer = 1

    this._speed = {
      x: 0,
      y: 0,
      a: 0.3,
      max: 15,
      k: {
        x: 1,
        y: 1
      }
    }
    // this._direction = Math.PI

    this.isCrossed = true
    this.attack = 2
  }

	// Позволяет оттолкнуть
	push(force) {
		if (this.extraForce) {
			this.extraForce.x += force.x * .1
			this.extraForce.y += force.y * .1
		} else {
			this.extraForce = force
		}
	}
	set extraForce(force) {
		if (!this._extraForce) {
			this._extraForce = { x: 0, y: 0 }
		}
		const MAX_FORCE = 20
		this._extraForce.x = typeof force?.x == 'number' && Math.abs(force.x) < MAX_FORCE ? force.x : this._extraForce.x
		this._extraForce.y = typeof force?.y == 'number' && Math.abs(force.y) < MAX_FORCE ? force.y : this._extraForce.y
	}
	get extraForce() {
		if (!this._extraForce) {
			this._extraForce = { x: 0, y: 0 }
		}
		return this._extraForce
	}
	calculateExtraForces() {
		if (Math.abs(this.extraForce.x) > 1) {
			this.extraForce.x -= Math.sign(this.extraForce.x) * 1
		} else {
			this.extraForce.x = 0
		}
		if (Math.abs(this.extraForce.y) > 1) {
			this.extraForce.y -= Math.sign(this.extraForce.y) * 1
		} else {
			this.extraForce.y = 0
		}
		if (!this.moveToPosition) {
			this.moveToPosition = {}
			this.moveToPosition.x = this.position.x + this.extraForce.x
			this.moveToPosition.y = this.position.y + this.extraForce.y
		}
	}

	lookTo(position) {
		if (!position) {
			return
		}
		this.direction = -Math.atan2( (-this.position.y + position.y), (-this.position.x + position.x))
	}
  moveTo(position, accuracy) {
    this.moveToPosition = position

    let pathX = Math.abs(position.x - this.position.x)
    let pathY = Math.abs(position.y - this.position.y)

    this._speed.k.x = pathX < pathY ? pathX / pathY : 1
    this._speed.k.y = pathX > pathY ? pathY / pathX : 1

    this.accuracy = accuracy || 1
  }

  // Метод, производящий выбор направления движения ~ исскуственный интеллект
  calculateForces() {
    if (!this.moveToPosition)
      return {
        x: 0,
        y: 0
      }

    let forceX = 0
    let forceY = 0

    if (Math.abs(this.moveToPosition.x - this.position.x) > this.accuracy) {
      if (this.moveToPosition.x > this.position.x) {
        forceX = this.moveToPosition.x - this.position.x > 1 ? 1 : .5
      } else {
        forceX = this.moveToPosition.x - this.position.x < -1 ? -1 : -.5
      }
    }

    if (Math.abs(this.moveToPosition.y - this.position.y) > this.accuracy) {
      if (this.moveToPosition.y > this.position.y) {
        forceY = this.moveToPosition.y - this.position.y > 1 ? 1 : .5
      } else {
        forceY = this.moveToPosition.y - this.position.y < -1 ? -1 : -.5
      }
    }

    return {
      x: this.extraForce.x + forceX,
      y: this.extraForce.y + forceY
    }
  }

  get speed() {
    return {
      x:this._speed.x,
      y:this._speed.y
    }
  }
  set speed(forces) {
    return this._speed
  }

  calculateSpeed(forces = {x: 0, y: 0}) {
    forces = {
      x: forces.x || 0,
      y: forces.y || 0
    }

    for (let key of ['x', 'y']) {
      // Множитель скорости
      let multiplier = this._speed.a + Math.abs(this._speed.a*this._speed[key])
      // Изменяем скорость
      this._speed[key] = this._speed[key] + forces[key] * multiplier

      // Если скорость больше максимальной, приравниваем ее максимальной
      if (Math.abs(this._speed[key]) > this._speed.max * this._speed.k[key])
          this._speed[key] = Math.sign(this._speed[key]) * this._speed.max * this._speed.k[key]

      // Если ускорения не заданы, постепенно уменьшаем скорость до 0
      if (!forces[key])
          this._speed[key] = Math.abs(this._speed[key]) > this._speed.a+.1 ? this._speed[key] - Math.sign(this._speed[key])* multiplier*.8 : 0
    }
  }

  calculatePosition(/*nearObjects = []*/) {
    // Коэфициент скорости
    let k = .16

    // Считаем новые предполагаемые координаты
    let newX = this._position.x + this.speed.x*k
    let newY = this._position.y + this.speed.y*k

    // Если объект не учавствует в пересечениях, то просто выдаем новые координаты
    if (!this.crossable) {
      this.cell = this.cell.level.getCell({ x: newX, y: newY })
      this._position.x = newX
      this._position.y = newY
      return
    }

    // TODO: свойство isLight нужно для определения пересечений. Стоит выделить как-то более явно
    let { position, cell } = this.cell.rayCast(this._position, { x: newX, y: newY }, this.isLight)
    // if (this.cell !== cell) { console.log(`${cell.x};${cell.y}`)  }

    this.cell = cell
    this._position.x = position.x
    this._position.y = position.y

    // Вызываем триггер пересечения со стенами (например) для пуль (например)
    if (position.x !== newX || position.y !== newY) {
      return this.crossTrigger()
    }
  }

  calculateDirection() {
    if (this.speed.x == 0 && this.speed.y == 0)
      return this._direction
    this._direction = -Math.atan2(this.speed.y, this.speed.x)
  }

  // Действия, производимые при перерисовке
  tic(nearObjects) {
		this.calculateExtraForces()
    let forces = this.calculateForces()
    this.calculateSpeed(forces)
    this.calculatePosition(nearObjects)
    this.calculateDirection()

    return super.tic(nearObjects)
  }
}
