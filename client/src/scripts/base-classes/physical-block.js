// import Intersect from './plugins/intersect.js'
import Performance from './../draw/performance.js'

let perf = Performance.counter()

/**
 * Двумерный вектор
 * @typedef {Object} Vector
 * @property {number} x - x-состовляющая вектора
 * @property {number} y - y-состовляющая вектора
 */

/**
 * Базовый класс рисуемого объекта
 * @class
 */
export default class PhysicalBlock {
  /**
   * Создание нового игрового персонажа
   * @param {{x: number, y: number, size: number, direction: number, id: string}} initialParameters - объект с параметрами инициализации рисуемого объекта

   */
	constructor(initialParameters = {x: 25, y: 25, size: 15, direction: 0, id: `${Math.round(Math.random()*1e6)}`}) {
    this.drawingLayer = 2

		this._position = {
			x: 'x' in initialParameters && typeof initialParameters.x == 'number' ? initialParameters.x : 25,
			y: 'y' in initialParameters && typeof initialParameters.y == 'number' ? initialParameters.y : 25
		}

		this.size = initialParameters.size
		this._direction = 'direction' in initialParameters && typeof initialParameters.direction == 'number' ? initialParameters.direction : 0

		// Истина в этом поле означает, что сквозь данный объект можно пройти
		// Ложь - происходит столкновение
		this.isCrossed = false

		// Свойство объекта, указывающее на то, что объект учавствует в обработке столкновений
		this.crossable = false
		// this.intersect = new Intersect(true, this.position,this._size)

		this.id = 'id' in initialParameters && typeof initialParameters.id == 'string' ? initialParameters.id : `${Math.round(Math.random()*1e6)}`
	}

	/**
	 * Получение объекта с текущими координатами объекта
	 * @name GET position
	 * @return {Vector} текущие координаты
	 *//**
	 * Установка новых координат объекту
	 * @name SET position
	 * @param {Vector} newPosition - новые координаты объекта
	 * @return {Vector} текущие координаты
	 */
	get position() {
		return {
			x:this._position.x,
			y:this._position.y
		}
	}
	set position({ x, y } = {x: 25, y: 25}) {
		this._position.x = typeof x == 'number' ? x : 25
		this._position.y = typeof y == 'number' ? y : 25

		return this._position
	}

	get direction() {
		return this._direction
	}
	set direction(newDirection = Math.PI) {
		this._direction = typeof newDirection == 'number' ? newDirection : 0
		return this._direction
	}

	get size() {
		return this._size
	}
	set size(value) {
		this._size = typeof value == 'number' && value >= 0 ? value : this._size ? this._size : 15
		return this._size
	}

	zoom(value) {
		this.globalSize = this.size - this.size * value
	}

	// Метод уничтожение экземляра. Посылает соответствующую команду во view-port
	desctruct() {
		this.deleted = true
	}

	// Действия, производимые при перерисовке
	tic() {
		let updatedParameters = {}

		return updatedParameters
	}

	// Срабатываемое событие пересечения
	crossTrigger(/*crossObject*/) {

	}
}
