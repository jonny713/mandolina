import PhysicalBlock from './physical-block.js'
import { ImageFrame } from './../draw/frame/image-frame'
import { AnimationFrame } from './../draw/frame/animation-frame'

class Graphic {
	addUI() {}
	addEffect() {}
	addGameObject() {}

	tic() {

	}
}




/**
 * @summary Класс содержит в себе все, что связано с отрисовкой объекта
 * Поскольку многие объекты будут генериться с помощью данных с сервера, нужно добавить в initialParameters инфу об этом
 * Там определяем поле style с объектом, в котором есть тип и прочие данные о внешнем виде
 */
export default class StyledBlock extends PhysicalBlock {
	constructor(initialParameters){
		super(initialParameters)

		let { style } = initialParameters

		// Скорее всего это по смыслу должно быть тут
		this.drawingLayer = 2

		// Свойство, содержащее всю инфу о графике (в том числе методы для работы с ней)
		// this.graphic
		this.frame = new ImageFrame(this, initialParameters.image)
		// this.frame = new ImageFrame(this)
	}

	// тут должен быть метод, который добавляет UI-элемент (полоска здоровья, флажок команды, прочее)

	// А тут добавление эффекта (циферка урона, шур-шур при ходьбеи прочее)
	// Может еще и пушки рисовать тут? Или пушка вообще должна быть как UI? Лучше сделать отдельно, все таки смысл разный

	// Действия, производимые при перерисовке
	tic() {

		super.tic()
		return this.frame
	}
}
