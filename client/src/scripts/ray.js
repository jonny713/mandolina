
/**
* Функция луча. На основании координат from, target и objects, возвращает true если target виден
*/
export default function Ray(from, target, targetType, objects) {
  let fromX = from.position.x
  let fromY = from.position.y

  let targetX = target.position.x
  let targetY = target.position.y

  // Расстояние между источником луча и целью
  let cSquare = (fromX - targetX) ** 2 + (fromY - targetY) ** 2

  for (let object of objects) {
    if (object.id === from.id || object.id === target.id)
      continue

    if (object.isCrossed)
      continue


    // // Квадрат расстояния между источником и объектом
    // let aSquare = (fromX - object.position.x) ** 2 + (fromY - object.position.y) ** 2
    //
    // // Квадрат расстояния между целью и объектом
    // let bSquare = (targetX - object.position.x) ** 2 + (targetY - object.position.y) ** 2
    //
    // if (aSquare > cSquare || bSquare > cSquare || aSquare + bSquare > cSquare * 1.5  )
    //   continue

    // // Полупериметр
    // let p = (a + b + c) / 2
    // let h = 2 * Math.sqrt(p) * Math.sqrt(p - a) * Math.sqrt(p - b) * Math.sqrt(p - c) / c
    //
    // // let h = ((x2 - x1) * (y0 - y1) - (y2 - y1) * (x0 - x1)) / c
    // // let h = Math.abs((targetX - fromX) * (object.position.y - fromY) - (targetY - fromY) * (object.position.x - fromX)) / c
    //
    // // console.log(object.constructor.name, `источник -> объект = ${a}; цель -> объект = ${b}; источник -> цель = ${c}; p = ${p};h = ${h};`);
    // // console.log(a, b,  c);
    // // console.log(`h = ${h}; size = ${object._size}; `);
    //
    // let intersect = object.intersect.withRay(from.position, target.position)
    // console.log('объект на пути луча', h < object._size * 2, intersect);

    if (object.intersect.withRay(from.position, target.position)) {
      return false
    }

    // if (h < object._size * 2) {
    //   // console.log('ОБЪЕКТ скрыт за ', object.constructor.name);
    //   return false
    // }


  }
  // console.log('ОБЪЕКТ ЗАМЕЧЕН');

  return Math.sqrt(cSquare)

}
