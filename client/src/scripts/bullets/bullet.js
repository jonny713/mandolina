import FractionBlock from './../fraction/fraction-block.js'

/**
* Базовый класс пули, реализующий основное поведение
*/
export default class Bullet extends FractionBlock {
  constructor(initialParameters, speed, acceleration, owner) {
    super(initialParameters)

    this.drawingLayer = 0
    // TODO: свойство нужно для определения пересечений. Стоит выделить как-то более явно
    this.isLight = true

    // this.intersect.isTransparent = true

    this.attack = 40
    this._speed.max = speed
    this._speed.a = acceleration
    this.size = 2

    // this.moveTo(target)
    this.moveForward()

		this.live = 0
    this.timeToLive = 5 * 1e3

    // setTimeout(() => {
    //   if (!this.deleted)
    //     this.desctruct()
    // }, this.timeToLive)

    // Сохраняем ссылку на владельца
    this.owner = owner
  }

  moveForward() {
    // console.log(this.direction);
    let target = {
      x: Math.cos(this.direction) * 1e7,
      y: -Math.sin(this.direction) * 1e7
    }
    this.moveTo(target)
  }

  desctruct() {
    // Порождаем эффект взрыва в текущих координатах


    super.desctruct()
  }

  calculatePosition() {
		let oldCell = this.cell
    super.calculatePosition()
    for (let near of this.fraction.enemies) {
			if (near instanceof Bullet) {
				continue
			}
      if (this.cell.compareWithObject(near, oldCell)) {
        this.crossTrigger(near)
      }
    }
  }

  // Срабатываемое событие пересечения
	crossTrigger(crossObject) {
		if (crossObject && typeof crossObject.takeDamage == 'function') {
			crossObject.takeDamage({ attack: this.attack }, this.owner)
		}
		if (!this.massDamage) {
			this.desctruct()
		}
	}

	tic(...args) {
		this.live++
		if (this.live > this.timeToLive) {
			this.desctruct()
		}

		return super.tic(...args)
	}
}
