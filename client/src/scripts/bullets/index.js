import Ammo from './../bullets/ammo.js'
import BadBigAmmo from './bad-big-ammo.js'

const bullets = {
	'classic': Ammo,
	'big': BadBigAmmo,
}

let bulletByName = (name) => bullets[name] || bullets.classic
export default bulletByName
