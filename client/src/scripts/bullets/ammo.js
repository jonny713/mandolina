import BasicItem from './../basic-item.js'
import Bullet from './bullet.js'

export default class Ammo extends BasicItem {
  constructor() {
    super()
    this.weight = 0
    this.associatedBullet = Bullet
  }
}
