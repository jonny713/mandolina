import Bullet from './bullet.js'
import { ImageTile } from './../draw/tiles/image'
import { TileMap } from './../draw/tiles/tile-map'

// let tile = new Tile()

export default class BadBigBullet extends Bullet {
	constructor(initialParameters, speed, acceleration, owner) {
		// TODO: это должно передаваться при создании, а не определяться в конструкторе
		initialParameters.image = new ImageTile(0, new TileMap({
			src: 'textures/splash.png',
			width: 100,
			height: 100,
			tileWidth: 100,
			tileHeight: 100,
		}))
		initialParameters.image.rotate = Math.PI / 2
		super(initialParameters, speed, acceleration, owner)

		this._speed.max = 16

		this.attack = 25

		this.size = 5

		this.timeToLive = 32

		this.massDamage = true

	}


	tic(...args) {
		this.size += 1.2
		return super.tic(...args)
	}

}
