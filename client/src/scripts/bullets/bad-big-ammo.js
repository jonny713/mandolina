import BasicItem from './../basic-item.js'
import BadBigBullet from './bad-big-bullet.js'

export default class BadBigAmmo extends BasicItem {
  constructor() {
    super()
    this.weight = 0
    this.associatedBullet = BadBigBullet
  }
}
