const list = [{
  id: 512,
  src: 'https://media.discordapp.net/attachments/316908045403684864/1055215918751436850/image.png',
  name: 'map 1',
  sprites: [{
    name: 'stone',
    cells: [[0,0],[0,1]],
    blocked: [[0,0],[0,1]],
  },{
    name: 'skul',
    cells: [[0,2],[1,2]],
    blocked: [[1,2]],
  },{
    name: 'grass3',
    cells: [[1,0]],
    blocked: [],
  }]
}, {
  id: 768,
  src: 'https://media.discordapp.net/attachments/316908045403684864/1055217259234213888/image.png',
  name: 'map 2',
  sprites: [{
    name: 'tree',
    cells: [[0,0],[1,0]],
    blocked: [[1,0]],
  },{
    name: 'grass1',
    cells: [[0,1]],
    blocked: [],
  },{
    name: 'grass2',
    cells: [[1,1]],
    blocked: [],
  }]
}]

const BASE_PATH = '/game/api'

export const getSpriteMapList = async () => {
  try {
    return await fetch(`${BASE_PATH}/sprite-maps/`, {
      method: 'GET',
    }).then(res => res.json())
      .then(res => {
        if (!res.success) {
          throw new Error(res.message)
        }
        return res.spriteMaps
      })
  } catch (e) {
    console.log(e);
    return list
  }
}


export const updateSpriteMapList = async (spriteMapId, formData) => {
  try {
    return await fetch(`${BASE_PATH}/sprite-maps/${spriteMapId}`, {
      method: 'POST',
      body: formData
    }).then(res => res.json())
      .then(res => {
        if (!res.success) {
          throw new Error(res.message)
        }
      })
  } catch (e) {
    console.log(e);
    throw new Error(e.message)
  }
}

export const createSpriteMapList = async (formData) => {
  try {
    return await fetch(`${BASE_PATH}/sprite-maps/`, {
      method: 'POST',
      body: formData
    }).then(res => res.json())
      .then(res => {
        if (!res.success) {
          throw new Error(res.message)
        }
      })
  } catch (e) {
    console.log(e);
    throw new Error(e.message)
  }
}
