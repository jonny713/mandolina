let protocol = location.protocol.replace('http', 'ws')
let hostname = location.hostname
let port = location.port == '8080' ? ':2002' :
location.port == '' ? '' :
':' + location.port

let wsConnection// = new WebSocket(`${protocol}//${hostname}${port}/game`);

function wsConnect() {
  wsConnection = new WebSocket(`${protocol}//${hostname}${port}/game`)

  wsConnection.onopen = () => console.log('ws:connected')

  wsConnection.onclose = () => {
    console.log('ws:reconnecting')
    wsConnect()
  }

  wsConnection.onerror = (e) => console.log(`ws:error:${e.message}`)

  wsConnection.onmessage = function(event) {
    const { type, ...data } = JSON.parse(event.data)
    // console.log(`ws:message:${type}`);

    if (callbacks[type]) {
      for (let cb of callbacks[type]) {
        cb(data)
      }
    }
  };
}

wsConnect()

const callbacks = {}

export const wsSend = function(data) {
  // Дожидаемся подключения
  if(!wsConnection.readyState){
    setTimeout(function (){
      wsSend(data);
    }, 100);
  } else {
    wsConnection.send(JSON.stringify(data));
  }
};

export const subscribe = (type, data) => {
  return {
    send: (data) => {
      wsSend({
        type,
        ...data,
      })
    },
    onMessage: (cb) => {
      if (typeof cb !== 'function') {
        return
      }

      if (!callbacks[type]) {
        callbacks[type] = []
      }

      callbacks[type].push(cb)
    },
  }
}
